<?php

use Illuminate\Http\Request;
use App\Http\Controllers\count;
use App\Http\Controllers\teams;
use App\Http\Controllers\sysrole;
use App\Http\Controllers\login;
use App\Http\Controllers\getteambyid;
use App\Http\Controllers\employees;
use App\Http\Controllers\employeetoteam;
use App\Http\Controllers\employeeteamrole;
use App\Http\Controllers\projects;
use App\Http\Controllers\search;
use App\Http\Controllers\getprojects;
use App\Http\Controllers\teamstoprojects;
use App\Http\Controllers\employeeroles;
use App\Http\Controllers\employeetoprojects;
use App\Http\Controllers\employeeprojectrole;
use App\Http\Controllers\checkroles;
use App\Http\Controllers\Kpis;
use App\Http\Controllers\filter;
use App\Http\Controllers\Evaluations;
use Illuminate\Support\Facades\Route;

//filter active employees.
Route::get('/active',[filter::class,"active"]);
//filter not active employees.
Route::get('/notactive',[filter::class,"notactive"]);
//filter in process projects.
Route::get('/inprocess',[filter::class,"inProcess"]);
//filter dne projects.
Route::get('/done',[filter::class,"done"]);
//filter employees for specific project.
Route::get('/filterproject',[filter::class,"project"]);

Route::resource('/getteam',getteambyid::class);
// add, edit, delete and get teams.
Route::resource('/team',teams::class);
//get systemroles.
Route::resource('/sysrole',sysrole::class);
//add, edit and get employees.
Route::resource('/employee',employees::class);
// get data for profile.
Route::get('/profile',[employees::class,'Profile']);
// get count emale and female.
Route::get('/gender',[employees::class,'getGender']);
//filter for emoloyees.
Route::resource('/search',search::class);
//filter for teams.
Route::get('/searchteam',[search::class,"searchTeams"]);
//filter for Roles.
Route::get('/searchrole',[search::class,"searchRoles"]);
//filter project by name.
Route::get('/searchproject',[search::class,"searchProjects"]);
//search kpi by name.
Route::get('/searchkpi',[search::class,"searchKpi"]);
//check username and password for login page.
Route::resource('/role',login::class);
//checking employees in this team so the team can not be deleted.
Route::resource('/findemployee',employeetoteam::class);
//count the number of employees and projects in this team.
Route::get('/countemployee',[employeetoteam::class,"countEmployeesAndProjects"]);
//get projects that is assgined to this team.
Route::get('/getprojectsforteams',[employeetoteam::class,"getProjectsforteams"]);
//get unassgined employees to teams.
Route::get('/unassginedemployees',[employeetoteam::class,"unassginedEmployees"]);
//update unassgined employee to join a team.
Route::put('/updateemployee/{id}',[employeetoteam::class,"updateEmployee"]);
Route::resource('/employeedata',employeeteamrole::class);
//count employees, projects, and teams.
Route::resource('/count',count::class);
// count employees in a role.
Route::get('/countemployeerole',[count::class,'countEmployeeRoles']);
// add, edit, delete and get projects.
Route::resource('/project',projects::class);
// get projects that does not ended yet and still in process.
Route::resource('/getprojects',getprojects::class);
//get and add for the table that have the realation between teams and projects.
Route::resource('/assginprojects',teamstoprojects::class);
//delete assgined teams to projects
Route::delete('/deleteassgin',[teamstoprojects::class,"deleteAssgin"]);
//add, edit, delete and get roles.
Route::resource('/roles',employeeroles::class);
// get all employees that belongs to this role id.
Route::get('/employeesroles',[employeeroles::class,'getAllEmployeeRole']);
// get projects for this employee in the team that is assgined to projects.
Route::resource('/findprojects',employeetoprojects::class);
//count the employees that are working in this project.
Route::get('/countemployeeproject',[employeetoprojects::class,"index"]);
//get employees that is assgined to this project.
Route::get('/getemployeeproject',[employeetoprojects::class,"getEmployees"]);
// get teams that is assgined to this team.
Route::get('/getteamproject',[employeetoprojects::class,"getTeams"]);
// assgin employees to projects and roles.
Route::resource('/assgin',employeeprojectrole::class);
//get employee role for a certian project.
Route::get('/getemployeeroles',[employeeprojectrole::class,'getEmployeeRole']);
// end the date of the role tthat is assgined to this employee.
Route::put('/updateemployeerole/{eid}/{pid}',[employeeprojectrole::class,'updateEmployeeRole']);
//check if role is assgined to employees so it con not be deleted.
Route::resource('/checkroles',checkroles::class);
// add, edit, delete and get kpi.
Route::resource('/kpi',Kpis::class);
//check kpi so it can not ne deleted.
Route::get('/checkkpi',[Kpis::class,'checkkpi']);
//get rating and date for graph.
Route::get('/evaluations',[Evaluations::class,'checkEvaluations']);
// add and get evalyations for employees.
Route::resource('/evaluation',Evaluations::class);
//get latest kpis for each employee.
Route::get('/chart',[Kpis::class,'getKpiEmployee']);
//check date for each kpi so thhe admin can noot add the same kpi in the same day.
Route::get('/checkdate',[Evaluations::class,'getEvaluationEmployee']);
//get employee project report.
Route::get('/report',[employeeprojectrole::class,'EmployeeReport']);
//count employees on tis team.
Route::get('/piechartteam',[count::class,"pieChartTeam"]);
// count active and not active.
Route::get('/piechartactive',[count::class,"pieChartActive"]);
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::get('/posts', function () {
//     return response()->json([
//         'posts' => [
//             'title' => 'Post is done'
//         ]
//     ]);
// });



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
