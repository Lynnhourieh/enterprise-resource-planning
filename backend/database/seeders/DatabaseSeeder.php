<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\SysRoles;
use App\Models\Employee;
use App\Models\Team;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        SysRoles::create([
            'sname'=>'employee'
        ]);
        SysRoles::create([
            'sname'=>'admin'
        ]);
        Team::create([
         'tname'=>'admin'
        ]);
        Employee::create([
            'fname'=>'Ahmad',
            'mname'=>'mohammad',
            'lname'=>'Jabak',
            'email'=>'ahmadjabak5@gmail.com',
            'phone'=>'76484640',
            'sys_id'=>2,
            'team_id'=>1,
            'username'=>'ahmadjabak',
            'password'=>'ahmadjabak123',
            'gender'=>'male',
            'isActive'=>'active'
        ]);
    }
}
