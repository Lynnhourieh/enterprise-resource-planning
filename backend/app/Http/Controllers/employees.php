<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class employees extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task = DB::table('employees');
        return $task->orderBy('isActive','ASC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //token
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $task = new Employee;
        if ($request->image) {
            $getImage = $request->image;
            $imageName = $getImage->getClientOriginalName();
            $imagePath = public_path() . '/images';
            $getImage->move($imagePath, $imageName);
            $task->image = $imageName;
        }


        $task->fname = $request->input('fname');
        $task->mname = $request->input('mname');
        $task->lname = $request->input('lname');
        $task->email = $request->input('email');
        $task->phone = $request->input('phone');
        $task->sys_id = $request->input('sys_id');
        $task->team_id = $request->input('team_id');
        $task->username = $request->input('username');
        $task->password = $request->input('password');
        $task->gender = $request->input('gender');
        $task->isActive = $request->input('isActive');

        $task->save(); //storing values as an object
        return $task;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Profile(Request $request)
    { 
        $teamid=$request["teamid"];
        $employeeid=$request["employeeid"];
        if($teamid){
        $task = DB::table('sys_roles')->join('employees', 'employees.sys_id', 'sys_roles.id')->join('teams', 'employees.team_id', 'teams.id')->where("employees.id", $employeeid)->where('teams.id',$teamid)->get();
        }else{
            $task = DB::table('sys_roles')->join('employees', 'employees.sys_id', 'sys_roles.id')->where("employees.id", $employeeid)->get(); 
        }
        return $task;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Employee::findorFail($id);

        if ($request->image) {
            $path = public_path() . '/images/';

            //code for remove old file
            if($task->image=null && $task->image=""){
                 $file_old = $path.$task->image;
                 unlink($file_old);
            }

            //upload new file
            $file = $request->image;
            $filename = $file->getClientOriginalName();
            $file->move($path, $filename);

            //for update in table
            $task->image = $filename;
           
        }
           $task->fname = $request['fname'];
           $task->mname = $request['mname'];
           $task->lname = $request['lname'];
           $task->email = $request['email'];
           $task->phone = $request['phone'];
           $task->sys_id = $request['sys_id'];
           $task->team_id = $request['team_id'];
           $task->username = $request['username'];
           $task->password = $request['password'];
           $task->gender = $request['gender'];
           $task->isActive = $request['isActive'];
        $task->update();
        //storing values as an object
        return response()->json([
            "updated",
            $request->all()

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function getGender()
    {
        $male=DB::table('employees')->where('gender','male')->count();
        $female=DB::table('employees')->where('gender','female')->count();
        return ['male'=>$male,'female'=>$female];
       
    }
}
