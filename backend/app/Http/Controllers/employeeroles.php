<?php

namespace App\Http\Controllers;
use App\Models\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class employeeroles extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task = new Roles();
        return $task->get();  //returns values in ascending order
    }

    public function getAllEmployeeRole(Request $request){
     $roleid=$request["id"];
     $task=DB::table("assgins")->select('fname','mname','lname','image','pname','assgins.startdate','assgins.enddate','isActive','assgins.employee_id','assgins.project_id')->join('employees','employees.id','assgins.employee_id')->join('projects','projects.id','assgins.project_id')->where('assgins.role_id',$roleid)->orderBy('assgins.enddate','ASC')->get();
     return $task;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Roles;
        $task->Rname = $request->input('Rname'); //retrieving user inputs
        $task->save(); //storing values as an object
        return $task; //returns the stored value if the operation was successful.
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Roles::findorFail($id); // uses the id to search values that need to be updated.
        $task->Rname = $request->input('Rname'); //retrieves user input
        $task->save(); //saves the values in the database. The existing data is overwritten.
        return response()->json($task);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Roles::findOrFail($id);
        $task->delete();
        return "Deleted succesfully";
    }
}
