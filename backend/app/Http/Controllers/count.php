<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Team;
use App\Models\Project;
use Illuminate\Support\Facades\DB;
class count extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task= Employee::all()->count();
        $task1= Team::all()->count();
        $task2= Project::all()->count();
        return response()->json([
          'employee'=> $task,
           'team'=> $task1,
            'project'=>$task2
        ]);
    }

    public function countEmployeeRoles(Request $request){
     $roleid=$request["id"];
     $task=DB::table('assgins')->select('employee_id')->where('role_id',$roleid)->count();
     return $task;
    }
    public function pieChartTeam(Request $request){

        $task= DB::table('employees')->select("tname as name",DB::raw('COUNT(team_id) as value'))->join('teams','teams.id','employees.team_id')->groupBY('team_id','tname')->get();

        return ["teams"=>$task];

    }
    public function pieChartActive(Request $request){

        $active= DB::table('employees')->where('isActive','active')->count();
        $notactive= DB::table('employees')->where('isActive','not active')->count();
        return ["active"=>$active,'not active'=>$notactive];

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
