<?php

namespace App\Http\Controllers;
use App\Models\Assgin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class employeeprojectrole extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $employeeid=$request['employee_id'];
        $projectid=$request['project_id'];
        $task=DB::table('assgins')->join('roles','roles.id','assgins.role_id')->where('assgins.employee_id',$employeeid)->where('project_id',$projectid)->where('enddate','=',null)->get();
        return $task;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Assgin();
        $task->employee_id = $request->input('employee_id');
        $task->project_id = $request->input('project_id');
        $task->role_id = $request->input('role_id');
        $task->startdate = $request->input('startdate'); 
        $task->enddate = $request->input('enddate');   //retrieving user inputs
        $task->save(); //storing values as an object
        return $task; //returns the stored value if the operation was successful.
    }
    public function getEmployeeRole(Request $request){
        $projectid=$request['project_id'];
        
        $task=DB::table('employees')->select('employees.id','employees.fname','employees.mname','employees.lname')->join('teams','employees.team_id','teams.id')->join('teamsprojects','teams.id','teamsprojects.team_id')->where("teamsprojects.project_id",$projectid)->whereNotIn('employees.id',DB::table('assgins')->select("employee_id")->where('assgins.enddate','=',null)->where('project_id',$projectid))->get();
        return ['message'=>$task];
    }
    public function updateEmployeeRole(Request $request,$pid,$eid){
       
       
           $enddate=$request["enddate"];
  
   $task= DB::table('assgins')
          ->where('employee_id',$eid)->where('project_id',$pid)
          ->update(['enddate' => $enddate
                    ]);       
           return ["success"=>$task];
          
       }
       
       public function EmployeeReport(Request $request){
       $employeeid=$request["employee_id"];
       $task=DB::table('assgins')->select('roles.rname','employees.fname','employees.mname','employees.lname','employees.image','projects.pname','projects.startdate','projects.enddate')->join('projects','projects.id','assgins.project_id')->join('roles','roles.id','assgins.role_id')->join('employees','employees.id','assgins.employee_id')->where('employees.id',$employeeid)->get();
       return ["message"=>$task];
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
