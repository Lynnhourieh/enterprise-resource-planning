<?php

namespace App\Http\Controllers;
use App\Models\kpi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class Kpis extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $task = new kpi();
        return $task->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    $task=new kpi;
        $task->kpiname = $request->input('kpiname');
        $task->save(); //storing values as an object
        return $task;}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task= kpi::findorFail($id);
        $task->get();
        return $task; 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task= kpi::findorFail($id);
        $task->kpiname = $request['kpiname'];
        $task->update();
        return response()->json([
            "updated",
            $request->all()

        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = kpi::findOrFail($id);
        $task->delete();
        return "Deleted succesfully";
    }
    public function getKpiEmployee(Request $request)
    {
     $employeeid=$request["employee_id"];
     $task=DB::table('evaluations AS E1')->join('kpis','E1.kpi_id','kpis.id')
     ->join(DB::raw('(SELECT max(edate) as maxDate,kpi_id,employee_id FROM evaluations where employee_id ='.$employeeid.' GROUP BY kpi_id,employee_id) E2'), 
     function($join)
     {
        $join->on('E1.employee_id', '=', 'E2.employee_id')->on('E1.kpi_id','=','E2.kpi_id')->on('E1.edate','=','E2.maxDate');
     })

     ->get();
     return $task;
    }
    public function checkkpi(Request $request)
    {
         $kpiid=$request["kpiid"];
        $task=DB::table("evaluations")->where("kpi_id",$kpiid)->get();
     
     return $task;
    }
}
