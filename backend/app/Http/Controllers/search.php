<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Team;
use App\Models\Project;
use App\Models\Roles;
use App\Models\kpi;
class search extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        if ($fname) {
            $task = DB::table('employees')->where('fname', 'Like','%'.$fname.'%')->orWhere('lname', 'Like','%'.$lname.'%')->get();
            return $task;
        } else {
            $employees = Employee::all();
            return $employees;
        }
    }

    public function searchTeams(Request $request)
    {
        $tname = $request['tname'];
        
        if ($tname) {
            $task = DB::table('teams')->where('tname', 'Like','%'.$tname.'%')->get();
            return $task;
        } else {
            $teams = Team::all();
            return $teams;
        }
    }

    public function searchProjects(Request $request)
    {
        
        $pname = $request['pname'];
        if ($pname) {
            $task = DB::table('projects')->where('pname', 'Like','%'.$pname.'%')->get();
            return $task;
        } else {
            $task = Project::all();
            return $task;
        }
    }
    public function searchRoles(Request $request)
    {
        
        $rname = $request['rname'];
        if ($rname) {
            $task = DB::table('roles')->where('rname', 'Like','%'.$rname.'%')->get();
            return $task;
        } else {
            $task = Roles::all();
            return $task;
        }
    }
    public function searchKpi(Request $request)
    {
        
        $kpiname = $request['kpiname'];
        if ($kpiname) {
            $task = DB::table('kpis')->where('kpiname', 'Like','%'.$kpiname.'%')->get();
            return $task;
        } else {
            $task = kpi::all();
            return $task;
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
