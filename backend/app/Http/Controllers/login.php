<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
class login extends Controller
{
    public function index(Request $request)
    {
        $token = Str::random(60);
        $username=$request->input('username');
        $password=$request->input('password');
        $task= DB::table('sys_roles')->join('employees','employees.sys_id','sys_roles.id')->where("employees.username",$username)->where("employees.password",$password)->where("employees.isActive","active")->where("sys_roles.sname","admin")->get();
       
        
       return['status'=>$task,'token'=>$token];
        
    }
    
}
