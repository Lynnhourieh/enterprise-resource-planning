<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
class employeetoprojects extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

       $projectid=$request["project_id"];
       $task=DB::table("employees")->join('teams','employees.team_id','teams.id')->join('teamsprojects','teams.id','teamsprojects.team_id')->where("teamsprojects.project_id",$projectid)->count();
       $task1= DB::table('teamsprojects')->join('teams','teamsprojects.team_id','teams.id')->where('project_id',$projectid)->get();
        return ["numberofemployees"=>$task,"teams"=>$task1];
    }


    public function getEmployees(Request $request)
    {

       $projectid=$request["project_id"];
       $task=DB::table("teamsprojects")->select('employees.id','employees.fname','employees.mname','employees.lname','employees.image','employees.isActive')->join('teams','teams.id','teamsprojects.team_id')->join('employees','employees.team_id','teams.id')->join('projects','teamsprojects.project_id','projects.id')->where("projects.id",$projectid)->get();
       
        return $task;
    }
    public function getTeams(Request $request)
    {

       $projectid=$request["project_id"];
       
       $task= DB::table('teamsprojects')->join('teams','teamsprojects.team_id','teams.id')->where('project_id',$projectid)->get();
        return ["message"=>$task];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $date=Carbon::now();
        $task=DB::table('employees')->join('teams','teams.id','employees.team_id')->join('teamsprojects','teamsprojects.team_id','teams.id')->join('projects','projects.id','teamsprojects.project_id')->select('teamsprojects.project_id','projects.pname')->where('employees.id',$id)->where('enddate','>=',$date)->where('status','in process')->get();
        return $task;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
