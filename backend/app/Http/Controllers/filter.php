<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class filter extends Controller
{
    public function active()
    {
        $users = DB::table('employees')->where('isActive', 'active')->get();
        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function notactive()
    {
        $users = DB::table('employees')->where('isActive', 'not active')->get();
        return $users;
    }

    public function done()
    {
        $users = DB::table('projects')->where('status', 'done')->get();
        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function inProcess()
    {
        $users = DB::table('projects')->where('status', 'in process')->get();
        return $users;
    }
    public function team(Request $id)
    {
        $team_id = $id->input('team_id');
        $task = DB::table('employees')->where("team_id", $team_id)->get();
        return $task;
    }
    public function project(Request $request)
    {
       
        $projectid=$request["project_id"];
       $task=DB::table("teamsprojects")->select('employees.id','employees.fname','employees.mname','employees.lname','employees.image','employees.isActive','employees.phone','employees.email')->join('teams','teams.id','teamsprojects.team_id')->join('employees','employees.team_id','teams.id')->join('projects','teamsprojects.project_id','projects.id')->where("projects.id",$projectid)->get();
        return $task;
    }

}
