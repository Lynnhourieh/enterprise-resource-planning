<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use Illuminate\Support\Facades\DB;
class projects extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
        $task = new Project();
        return $task->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       
        $task = new Project;
        $task->pname = $request->input('pname');
        $task->discription = $request->input('discription');
        $task->startdate = $request->input('startdate');
        $task->enddate = $request->input('enddate');
        $task->status = $request->input('status'); //retrieving user inputs
        $task->save(); //storing values as an object
        return $task;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task= Project::findorFail($id);
        $task->get();
        return $task; 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Project::findorFail($id); // uses the id to search values that need to be updated.
        $task->pname = $request->input('pname');
        $task->discription = $request->input('discription'); 
        $task->startdate = $request->input('startdate');
        $task->enddate = $request->input('enddate'); 
        $task->status = $request->input('status');   //retrieves user input
        $task->save(); //saves the values in the database. The existing data is overwritten.
        return response()->json($task);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Project::findOrFail($id);
        $task->delete();
        return "Deleted succesfully";
    }
}
