<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\evaluation;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class Evaluations extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new evaluation;
        $task->kpi_id = $request->input('kpi_id');
        $task->employee_id = $request->input('employee_id');
        $task->feedback = $request->input('feedback');
        $task->edate = $request->input('edate');
        $task->rating = $request->input('rating');
       
        $task->save(); //storing values as an object
        return $task; //returns the stored value if the operation was successful.
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getEvaluationEmployee(Request $request)
    {
        $kpiid=$request["kpiid"];
        $employeeid=$request["employeeid"];
        $date=Carbon::now()->format('y-m-d');
      $task=DB::table('evaluations')->where('kpi_id',$kpiid)->where('employee_id',$employeeid)->where('edate',$date)->get();
       return ['date'=>$task];
    }
    public function checkEvaluations(Request $request)
    {
        $id=$request['id'];
        $employeeid=$request['employeeid'];
      $task=DB::table('evaluations')->join('kpis','kpis.id','evaluations.kpi_id')->where('kpiname',$id)->where('employee_id',$employeeid)->get();
       return $task;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
