<?php

namespace App\Http\Controllers;
use App\Models\teamsprojects;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class teamstoprojects extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $teamid=$request['team_id'];
        $projectid=$request['project_id'];

        $task= DB::table('teamsprojects')->where('team_id',$teamid)->where('project_id',$projectid)->get();
        $task1=DB::table('projects')->where('id',$projectid)->get();
        return["status"=>$task,
                "get"=>$task1];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
         
       
        $task = new teamsprojects();
        $task->team_id = $request['team_id'];
        $task->project_id=$request['project_id']; //retrieving user inputs
        $task->save(); //storing values as an object
       return $task;
       
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }

    public function deleteAssgin(Request $request){
        $teamid=$request['team_id'];
        $projectid=$request['project_id'];
        $task = DB::table('teamsprojects')->where('team_id',$teamid)->where('project_id',$projectid)->delete();
        
        return "Deleted succesfully";
    }
}
