<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Employee;
class employeetoteam extends Controller
{
    public function index(Request $request)
    {
        
        $team_id=$request->input('team_id');
        $task= DB::table('employees')->where('team_id',$team_id)->get();

        
       return $task;
        
    }
    public function countEmployeesAndProjects(Request $request){
        $team_id=$request->input('team_id');
        $task= DB::table('employees')->where('team_id',$team_id)->count();
        $date=Carbon::now();
        $task1=DB::table('teamsprojects')->join('projects','projects.id','teamsprojects.project_id')->where('teamsprojects.team_id',$team_id)->where('projects.enddate','>=',$date)->where('status','in process')->count();
        
       return ['numberofemployees'=>$task, 'numberofprojects'=>$task1];
    }
    public function getProjectsforteams(Request $request){
        $team_id=$request->input('team_id');
        $date=Carbon::now();
        $task=DB::table('teamsprojects')->join('projects','projects.id','teamsprojects.project_id')->where('teamsprojects.team_id',$team_id)->where('projects.enddate','>=',$date)->where('status','in process')->get();
        return ["message"=>$task];

    }
public function unassginedEmployees(){
    $task=DB::table('employees')->where('team_id',null)->get();
 
    return ['message'=>$task];

}
public function updateEmployee(Request $request, $id){
 $team_id=$request->only('team_id');
   
$task=Employee::FindorFail($id);
$tasks = $task->where('id', $id)->update($team_id);
    
    // $task->update(["team_id",$team_id]);

    
    return ["success"=>$tasks];
   
   
  
}
}
