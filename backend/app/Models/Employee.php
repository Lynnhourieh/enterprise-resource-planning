<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    // public function sysrole()
    // {
    //     return $this->hasMany(SysRoles::class,'id','sys_id');
    // }
    protected $fillable = ["image", "fname", "mname", "lname", "email", "phone", "sys_id", "team_id", "username", "password", "isActive"];
}
