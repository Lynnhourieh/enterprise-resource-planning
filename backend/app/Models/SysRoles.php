<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SysRoles extends Model
{
    use HasFactory;
  
    public function role()
    {
        return $this->belongsTo(Employee::class);
    }
    
}
