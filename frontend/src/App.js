import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Routes,
} from "react-router-dom";
import LoginPage from './components/login/LoginPage';
import Employee from './components/Employee/Employee';
import Team from './components/Team/Team';
import Profile from './components/Profile/profile';
import Project from './components/Projects/Project';
import "./Page.css"
import { useState } from "react";
import MainLayout from './components/Layout/MainLayout';
import Roles from "./components/Roles/roles";
import Kpi from "./components/Kpi/Kpi"
import TeamDetails from './components/Team Details/TeamDetails';
import ProfileEdit from './components/Profile/ProfileEdit';
import Dashboard from './components/Dashboard/dashboard';
import ProjectDetails from "./components/Project Details/ProjectDetails"
import RoleDetails from './components/Role Details/RoleDetails';
function App() {

  return (
    <Router>
      
          <Routes>
          <Route path='/' element={<MainLayout />}>
              <Route index element={<Dashboard />} />
            </Route>
            <Route path='/employees' element={<MainLayout />}>
              <Route index element={<Employee />} />
            </Route>
            <Route path='/teams' element={<MainLayout />}>
              <Route index element={<Team />} />
            </Route>
            <Route path='/teamdetails' element={<MainLayout />}>
              <Route index element={<TeamDetails />} />
            </Route>
            <Route path='/profile' element={<MainLayout  />}>
              <Route index element={<Profile/>} />
              
            </Route>
            <Route path="/login" element={<LoginPage />} />
            <Route path='/projects' element={<MainLayout />}>
              <Route index element={<Project />} />
            </Route>
            <Route path='/projectdetails' element={<MainLayout />}>
              <Route index element={<ProjectDetails />} />
            </Route>
            <Route path='/roles' element={<MainLayout />}>
              <Route index element={<Roles />} />
            </Route>
            <Route path='/roledetails' element={<MainLayout />}>
              <Route index element={<RoleDetails />} />
            </Route>
            <Route path='/kpi' element={<MainLayout />}>
              <Route index element={<Kpi />} />
            </Route>
          </Routes>
        
        
      
    </Router>
  );
}

export default App;
