import React from 'react'
import NavForAll from '../NormalNavBar/NavForAll'
import LoginForm from './loginForm'
function LoginPage({ setId }) {
  return (
    <>
      <NavForAll />
      <LoginForm setId={setId} />
    </>
  )
}

export default LoginPage