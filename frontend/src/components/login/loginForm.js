import React from 'react'
import "./loginForm.css"
import { useState, useEffect } from 'react';
import { Link, useNavigate } from "react-router-dom";
import Popup from '../Popup/Popup';
import YouCantDelete from '../Team/YouCantDelete';
function LoginForm() {
  const [data, setData] = useState('');
  const [username, setUser] = useState('');
  const [password, setPass] = useState('');
  const navigate = useNavigate();
  const [show, setShow] = useState(false);
  const showPopup = () => {
    setShow(true);
  };
  const closeHandler = () => {
    setShow(false);
    document.body.style.close = "auto";
  };
  const onSubmitHandler = (e) => {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_ERP_URL}/api/role?username=${username}&password=${password}`, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        } throw reponse;
      }).then(data => {
        setData(data)
       
        if (data.status != "") {
          localStorage.setItem('id',data.status[0].id)
          localStorage.setItem('token', data.token)
          navigate('/');

        } else {
          return showPopup();
        }
      }, [])
  };


  return (
    <div className='loginform'>
      <form>
        <h3>Login</h3>
        <h2>Login to access your account</h2>
        <label for="username">Username:</label>
        <input type="text" placeholder="Username" id="username" value={username} onChange={(e) => { setUser(e.target.value) }} />

        <label for="password">Password:</label>
        <input type="password" placeholder="Password" id="password" value={password} onChange={(e) => { setPass(e.target.value) }} />

        <button onClick={onSubmitHandler}>SIGN IN</button>
      </form>
      <Popup
        show={show}
        setShow={setShow}
        title=""
        content={
          <YouCantDelete
            content="Incorrect Username and Password."
            closeHandler={closeHandler}
          />
        }
      />
    </div>

  )
}

export default LoginForm