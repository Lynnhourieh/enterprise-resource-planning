import React, { useEffect, useState } from 'react';
import './sidebar.css';
import { RiDashboardFill, RiTeamLine } from 'react-icons/ri';
import { BsConeStriped, BsPersonCircle } from 'react-icons/bs';
import { MdOutlineWork } from 'react-icons/md';
import { HiMenu } from 'react-icons/hi';
import { useNavigate } from "react-router-dom";
import FormatListBulletedIcon from '@mui/icons-material/FormatListBulleted';
import BadgeIcon from '@mui/icons-material/Badge';
import StarRateIcon from '@mui/icons-material/StarRate';



import { Link } from "react-router-dom";
const isNarrowScreen = window.matchMedia("(max-width: 767px)").matches;
const notNarrowScreen = window.matchMedia("(min-width: 767px)").matches;

export default function Sidebar() {
  const [isOpen, setIsOpen] = useState(false);
  const [loading, setloading] = useState(true);

  const [singleEmployee, setSingleEmployee] = useState([]);


  const navigate = useNavigate();
  const Profile = () => {
    navigate('/profile', { state: { id: singleEmployee[0].id } })
    window.location.reload(false)
  }

  const GetEmployee = () => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/profile?employeeid=${localStorage.getItem('id')}`, {

      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        } throw reponse;
      })
      .then(data => {
        setSingleEmployee(data)

        setloading(false);
      })

  }


  useEffect(() => {
    GetEmployee();
  }, [])

  if (loading) return "loading...";


  const handleTrigger = () => setIsOpen(!isOpen);
  if (isNarrowScreen) {
    return (

      <div className='admin-dash'>
        <div className={`sidebar ${isOpen ? "sidebar--open" : ""}`}>
          
          <div className="trigger" onClick={handleTrigger}>
            <HiMenu />
          </div>
          <div class="top-sidebar">
            <img src={`${process.env.REACT_APP_ERP_URL}/images/${singleEmployee[0].image}`} alt="" className="adminImg" />
          </div>
          <div className="sidebar-position1" onClick={Profile}>
            <span >{singleEmployee[0].fname} {singleEmployee[0].lname}</span>
          </div>
          <Link to='/' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <RiDashboardFill className='sidebar-icon' />
              <span >Dashboard</span>
            </div></Link>
          <Link to='/employees' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <BsPersonCircle className='sidebar-icon' />
              <span >Employees</span>
            </div></Link>
          <Link to='/teams' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <RiTeamLine className='sidebar-icon' />
              <span >Teams</span>
            </div>
          </Link>
          <Link to='/projects' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <MdOutlineWork className='sidebar-icon' />
              <span >Projects</span>
            </div>
          </Link>
          <Link to='/kpi' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <StarRateIcon className='sidebar-icon' />
              <span >KPI's</span>
            </div>
          </Link>
          <Link to='/roles' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <FormatListBulletedIcon className='sidebar-icon' />
              <span >Roles</span>
            </div>
          </Link>
        </div>
      </div>
    )
  } else if (notNarrowScreen) {
    return (
      <div className='admin-dash'>
        <div className={`sidebar ${!isOpen ? "sidebar--open" : ""}`}>
          <div className="trigger" onClick={handleTrigger}>
            <HiMenu />
          </div>
          <div class="top-sidebar">
            <img src={`${process.env.REACT_APP_ERP_URL}/images/${singleEmployee[0].image}`} alt="" className="adminImg" />
          </div>
          <div className="sidebar-position1" onClick={Profile}>
            <span >{singleEmployee[0].fname} {singleEmployee[0].lname}</span>

          </div>
          <Link to='/' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <RiDashboardFill className='sidebar-icon' />
              <span >Dashboard</span>
            </div></Link>
          <Link to='/employees' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">

              <BsPersonCircle className='sidebar-icon' />

              <span >Employees</span>
            </div></Link>
          <Link to='/teams' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <RiTeamLine className='sidebar-icon' />
              <span >Teams</span>
            </div>
          </Link>

          <Link to='/projects' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <MdOutlineWork className='sidebar-icon' />
              <span >Projects</span>
            </div>
          </Link>

          <Link to='/kpi' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <StarRateIcon className='sidebar-icon' />
              <span >KPI's</span>
            </div>
          </Link>

          <Link to='/roles' style={{ color: 'inherit', textDecoration: 'inherit' }} className='link'>
            <div className="sidebar-position">
              <FormatListBulletedIcon className='sidebar-icon' />
              <span >Roles</span>
            </div>
          </Link>
        </div>
      </div>
    )
  }
}