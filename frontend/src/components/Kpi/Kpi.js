import React from "react";
import KpiName from "./KpiName";
import GroupAddIcon from "@mui/icons-material/GroupAdd";
import Popup from "../Popup/Popup";
import { useState, useEffect } from "react";
import KpiAdd from "./KpiAdd";
import Search from "../Search/Search";
import "./Kpi.css";



function Kpi() {
  const [data, setData] = useState("");
  const [loading, setloading] = useState(true);
  const [show, setShow] = useState(false);
  const [showedit, setShowEdit] = useState(false);
  const [kpiname, setKpiname] = useState('')
  const [kpiId, setKpiId] = useState('')
  const showPopup = () => {
    setShow(true);

  };
  const showPopupEdit = (kpiname, id) => {
    setShowEdit(true)
    setKpiId(id)
    setKpiname(kpiname)
  }
  const closeHandler = () => {
    setShow(false);
    setShowEdit(false)
    document.body.style.close = "auto";
  };
  useEffect(() => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/kpi`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        // throw reponse;
      })
      .then((data) => {
        setloading(false);
        setData(data);

      });
  }, []);
  const onSearch = async (e, search) => {
    e.preventDefault();
    await fetch(
      `${process.env.REACT_APP_ERP_URL}/api/searchkpi?kpiname=${search}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };

  if (loading) return "loading...";
  return (
    <>
      <div className="container">

        <div className="projectheader">
          <b>KPI</b>
        </div>
        <Search onSearchHandler={onSearch}/>



        <button className="button-68 kpi" role="button" onClick={() => showPopup()}>
          <GroupAddIcon />

        </button>

<div className="three">
        {data.map((datas) => (
          <KpiName kpiname={datas.kpiname} id={datas.id} showPopupEdit={showPopupEdit} />
        ))}
</div>

      </div>

      <Popup
        show={show}
        overlay={true}
        content={
          <KpiAdd
            closeHandler={closeHandler}

          />
        }
      />
      <Popup
        show={showedit}
        overlay={true}
        content={
          <KpiAdd
            closeHandler={closeHandler}
            kpiId={kpiId}
            kpiname={kpiname}
            setKpiname={setKpiname}
          />
        }
      />
    </>
  );
}

export default Kpi;
