import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
function KpiAdd({ closeHandler,kpiId,kpiname,setKpiname }) {
   const [kpinames, setKpinames] = useState(kpiname);
  const onSubmitHandler = () => {
    const formData = new FormData();
    formData.append("kpiname", kpinames);
    axios
      .post(`${process.env.REACT_APP_ERP_URL}/api/kpi`, formData, {
        method: "POST",
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => console.log("Successfuly Sent!"))
      .catch((err) => {
        console.log(err);
      });
  };
  
const onUpdatekpi = (e) => {
    e.preventDefault();
    
     
       axios
         .put(`${process.env.REACT_APP_ERP_URL}/api/kpi/${kpiId}?kpiname=${kpiname}`, {
 
           headers: { 'Content-Type': 'multipart/form-data' },
 
         })
         .then((res) =>{ console.log('Successfuly Sent!')
       window.location.reload(false)})
         .catch((err) => {
 
           console.log(err);
         });
   
   }
  

   if(!kpiId){ return (
    <div class="login-box">
      <span className="close" onClick={closeHandler}>
        &times;
      </span>

      <h1>KPI</h1>
      <hr></hr>
      <form >
        <div class="user-box">
          <input
            type="text"
            
            value={kpinames}
            onChange={(e) => {
              setKpinames(e.target.value);
            }}
            required
          />
          <label>Add Kpi </label>
        </div>

        <button class="button-6" role="button" onClick={onSubmitHandler}>
          ADD
        </button>
      </form>
    </div>
  );}else if(kpiId){ return (
    <div class="login-box">
      <span className="close" onClick={closeHandler}>
        &times;
      </span>

      <h1>KPI</h1>
      <hr></hr>
      <form >
        <div class="user-box">
          <input
            type="text"
            
            value={kpiname}
            onChange={(e) => {
              setKpiname(e.target.value);
            }}
            required
          />
          <label>Edit Kpi </label>
        </div>

        <button class="button-6" role="button" onClick={onUpdatekpi}>
          Edit
        </button>
      </form>
    </div>)}
 
}

export default KpiAdd;
