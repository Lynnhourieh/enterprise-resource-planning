import React from "react";
import { useState, useEffect } from "react";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import axios from "axios";
import Popup from "../Popup/Popup";
import { useLocation } from "react-router-dom";
import KpiAdd from "./KpiAdd";
import YouCantDelete from "../Team/YouCantDelete";
import DeleteConfermation from "../Team/DeleteConfermation";
function KpiName({ kpiname, id, showPopupEdit }) {
  const location = useLocation();
  const [showdelete, setShowDelete] = useState(false);
  const [showcant, setShowCant] = useState(false);
  const [show, setShow] = useState(false);
  const [data, setData] = useState("");
  const[check,setCheck]=useState('')
  const showPopupDelete = () => {
    setShowDelete(true);

  };

  const showPopupCant = () => {
    setShowCant(true);

  };
  const checkKpi = async () => {
      
    try {
      const response = await axios.get(`${process.env.REACT_APP_ERP_URL}/api/checkkpi?kpiid=${id}`);
      const { data: check } = response;
      setCheck(check)
      if(check!=""){
        showPopupCant()
      }else{
        showPopupDelete()
      }   
    } catch (error) {
      console.log(error.message);
    }
    
  };
 

  const closeHandlerCant = () => {
    setShowCant(false);
    document.body.style.close = "auto";
  };
  const closeHandlerDelete = () => {
    setShowDelete(false);
    document.body.style.close = "auto";
  };
  const onSubmitHandlerDelete = async (e) => {
    
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/kpi/${id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        window.location.reload();
        if (response.ok) {
          return response.json();
        }
        throw response;
      })
      .then((data) => {
        setData(data);
      });
  };

   return (
    
    <div class="cards-list">

      <div class="card 1" >
      
        
        <div class="card_title title-white">
         
          {/* <div class="card_image"> </div> */}
          <p>{kpiname}</p>
          
          <div class="edit-delete-button" >
          <div class="lines" onClick={checkKpi}>
             <DeleteIcon sx={{ color: "black", fontSize: 30 }} />
           </div>
          <div class="lines" onClick={() => showPopupEdit(kpiname,id)} >
            <EditIcon  sx={{ color: "black", fontSize: 30 }} />
           </div>
           </div>
        </div>
      </div>
    
    {/* //<div class="container">




  // <div class="col-12 col-sm-6 col-md-4 col-lg-3">

  //   <div class="our-team">

      
  //     <div class="team-content" >
  //       <h3 className="nameemployee">{kpiname}</h3>
  //       <h4 class="nameemployee" ></h4>
       
  //       <h5 class="titleemployee">
  //         <div class="delete-button" onClick={checkKpi}>
  //           <DeleteIcon sx={{color:'#57bee6'}}/>
  //         </div>
  //         <div class="col col-4 ">
  //           <EditIcon  sx={{color:'#57bee6'}}onClick={() => showPopupEdit(kpiname,id)} />
  //         </div>
          
  //       </h5>

  //     </div>

  //   </div>

  // </div> */}
  <Popup
        show={showdelete}
        overlay={false}
        content={
          <DeleteConfermation
            closeHandlerDelete={closeHandlerDelete}
            onDeleteTeam={onSubmitHandlerDelete}
            content="Are You Sure You Want To Delete This KPI."




          />
        }
      />
  <Popup
        show={showcant}
        overlay={false}
        content={
          <YouCantDelete
            closeHandler={closeHandlerCant}
            content="This KPI Has Employees You Can't Delete It."

          />
        }
      />

</div>
  
  );
}

export default KpiName;
