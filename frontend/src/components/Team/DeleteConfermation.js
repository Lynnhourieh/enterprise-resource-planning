import React from 'react'
import './confermationdelete.css';
function DeleteConfermation({closeHandlerDelete,onDeleteTeam,content}) {
  return (
    <div className='confermation'>
        <span className="close" onClick={closeHandlerDelete}>
          &times;
        </span>
        
   <h1>{content}</h1>
   <div className='button'>
   <button className='button-confirm' onClick={onDeleteTeam}>OK</button>
   </div>
   </div>
  )
}

export default DeleteConfermation