import "./team.css";
import TeamName from "./TeamName";
import { useState, useEffect } from "react";
import GroupAddIcon from '@mui/icons-material/GroupAdd';

import TeamAddEdit from "./TeamAddEdit";
import Popup from "../Popup/Popup";
import AssginProjects from "../Team Details/AssginProjects";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Search from "../Search/Search";
function Team() {
  const [data, setData] = useState([]);
  const [loading, setloading] = useState(true);
  const [show, setShow] = useState(false);
  const [showedit, setShowEdit] = useState(false);

  
  const [id,setId]=useState("")
 
  const [tname,setTname]=useState("")
  const navigate = useNavigate();
  const showPopup = () => {
    setShow(true);
    
  };
  const showPopupEdit=(id,tname)=>{
    setShowEdit(true)
    setId(id)
    setTname(tname)
  }
  const closeHandler = () => {
    setShow(false);
    setShowEdit(false)
   
    document.body.style.close = "auto";
  };
 
  useEffect(() => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/team`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setloading(false);
        setData(data);
      });
  }
 
  , []);
  useEffect(()=>{
    const token=localStorage.getItem('token');
      if(!token) {
        navigate('/login');
      }
  },[])
  const onSearchHandler = async (e,search) => {
    
    e.preventDefault();
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/searchteam?tname=${search}`, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        }
      }).then(data => {
        setData(data)
      })
  }
  
  if (loading) return "loading...";

  return( <>
    <div class="container">
   
    
        
        <div className="projectheader"><b>Teams</b></div>
        <Search onSearchHandler={onSearchHandler}/>
        <div className="line">
       
        <GroupAddIcon sx={{fontSize:40}} onClick={showPopup}/>
      </div>
  </div>
  <div className="team-cards">
    { 
           data.map((datas) => 
            <TeamName key={datas.id} id={datas.id} tname={datas.tname} showPopupEdit={showPopupEdit} /> 
          )
        } 
        </div>
       <Popup
          show={show}
          title=""
          content={
            <TeamAddEdit closeHandler={closeHandler}
           
            />
          }
        />
         <Popup
          show={showedit}
          title=""
          content={
            <TeamAddEdit closeHandler={closeHandler}
            id={id}
            tname={tname}
            setTname={setTname}
            />
          }
        />

    
  </>);
}

export default Team;
