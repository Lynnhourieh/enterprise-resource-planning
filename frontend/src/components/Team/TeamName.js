import "./team.css";
import React from "react";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Popup from "../Popup/Popup";
import { useEffect, useState } from "react";
import TeamAddEdit from "./TeamAddEdit";
import DeleteConfermation from "./DeleteConfermation";
import YouCantDelete from "./YouCantDelete";
import AssginProjects from "../Team Details/AssginProjects";
import axios from "axios";
import { useNavigate } from "react-router-dom"

function TeamName({ tname, id, showPopupEdit }) {
  const [showdelete, setShowDelete] = useState(false);
  const [showcant, setShowCant] = useState(false);
  const [count, setCount] = useState('')
  const [data, setData] = useState('');

  const navigate = useNavigate();
  const teamDetails = () => {
    navigate('/teamdetails', { state: { id: id, tname: tname } })

  }

  const showPopupDelete = () => {
    setShowDelete(true);

  };

  const showPopupCant = () => {
    setShowCant(true);

  };
  const closeHandlerCant = () => {
    setShowCant(false);
    document.body.style.close = "auto";
  };

  const closeHandlerDelete = () => {
    setShowDelete(false);
    document.body.style.close = "auto";
  };
  const onSubmitHandler = (e) => {

    fetch(`${process.env.REACT_APP_ERP_URL}/api/findemployee?team_id=${id}`, {

      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        } throw reponse;
      }).then(data => {
        setData(data)
        if (data != "") {
          showPopupCant();

        } else {
          return showPopupDelete();
        }
      }, [])
  };

  const onDeleteTeam = (e) => {

    axios
      .delete(`${process.env.REACT_APP_ERP_URL}/api/team/${id}`, {

        headers: { 'Content-Type': 'multipart/form-data' },


      })
      .then((res) => {
        console.log('Successfuly Sent!')
        window.location.reload(false)
      })
      .catch((err) => {

        console.log(err);
      });
  };
  const CountEmployeesandProjects = async () => {

    if (id) {

      try {
        const response = await axios.get(`${process.env.REACT_APP_ERP_URL}/api/countemployee?team_id=${id}`);
        const { data: count } = response;
        setCount(count)

      } catch (error) {

        console.log(error.message);
      }
    }
  };


  useEffect(() => {

    CountEmployeesandProjects()

  }, [])

  return (
    <div class="container" >
      <div class="container">

        <div class="col-12 col-sm-6 col-md-4 col-lg-3">

          <div class="our-team">
<div onClick={teamDetails}>
            <div class="picture">
              <img src="https://assets.entrepreneur.com/content/3x2/2000/20151215195453-business-leader-group-front-leadership-team-professionals-businesspeople.jpeg?crop=4:3" />
            </div>
            <div class="team-content" >
              <h3 class="nameemployee"  >{tname}</h3>
              <h4 class="titleemployee">Employees: {count.numberofemployees}</h4>
              <h4 class="titleemployee">Projects: {count.numberofprojects}</h4>


            </div></div>
            <div class="icons ">
              
                <DeleteIcon onClick={onSubmitHandler} />
              
              
                <EditIcon onClick={() => showPopupEdit(id, tname)} />
            

            </div>
          </div>

        </div>


      </div>


      <Popup
        show={showdelete}
        overlay={true}
        content={
          <DeleteConfermation
            closeHandlerDelete={closeHandlerDelete}
            onDeleteTeam={onDeleteTeam}
            content="Are You Sure You Want To Delete This Team."




          />
        }
      />

      <Popup
        show={showcant}
        overlay={true}
        content={
          <YouCantDelete
            closeHandler={closeHandlerCant}
            content="This Team Has Employees You Can't Delete It."

          />
        }
      />


    </div>

  );
}

export default TeamName;
