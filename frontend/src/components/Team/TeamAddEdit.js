import "./teamAddEdit.css";
import React from "react";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import axios from "axios";
function TeamAddEdit({closeHandler,id,tname,setTname}){
   const [tnames, setTnames] = useState([]);
 
 
  const onSubmitHandler = (e) => {
   
    const formData = new FormData();
    formData.append("tname", tnames);
    axios
      .post(`${process.env.REACT_APP_ERP_URL}/api/team`, formData, {
       
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => console.log("Successfuly Sent!"))
      .catch((err) => {
        console.log(err);
      });
  };
 
  

  const onUpdateTeams = (e) => {
   e.preventDefault();
   
    
      axios
        .put(`${process.env.REACT_APP_ERP_URL}/api/team/${id}?tname=${tname}`, {

          headers: { 'Content-Type': 'multipart/form-data' },

        })
        .then((res) =>{ console.log('Successfuly Sent!')
      window.location.reload(false)})
        .catch((err) => {

          console.log(err);
        });
  
  }

  

if(!id){
    return(
    <div className="login-box">
    
       <span className="close" onClick={closeHandler}>
          &times;
        </span>
    <h1>Add New Team</h1>
    <hr></hr>
    <form>
      <div class="user-box">
        <input type="text" name="" value={tnames} onChange={(e) => {
                setTnames(e.target.value);
              }}
              required/>
        <label>Team name</label>
      </div>
     
      <button class="button-6" role="button" onClick={onSubmitHandler}>Add</button>
      
        
      
    </form>
  </div>
  )}else if(id){
    return(
      <div className="login-box">
      
         <span className="close" onClick={closeHandler}>
            &times;
          </span>
      <h1>Add New Team</h1>
      <hr></hr>
      <form>
        <div class="user-box">
          <input type="text" name="" value={tname} onChange={(e) => {
                  setTname(e.target.value);
                }}
                required/>
          <label>Team name</label>
        </div>
       
        <button class="button-6" role="button" onClick={onUpdateTeams}>EDIT</button>
        
          
        
      </form>
    </div>
    )
  }
}

export default TeamAddEdit