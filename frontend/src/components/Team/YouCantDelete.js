import React from 'react'

function YouCantDelete({closeHandler,content}) {
  return (
    <div className='confermation'>
    <span className="close" onClick={closeHandler}>
      &times;
    </span>
    
<h1>{content}</h1>
<div className='button'>
   <button className='button-confirm' onClick={closeHandler}>OK</button>
   </div>
</div>
  )
}

export default YouCantDelete