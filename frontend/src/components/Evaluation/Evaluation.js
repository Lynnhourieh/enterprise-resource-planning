import "./Evaluation.css";
import React from "react";
import PersonAddIcon from "@mui/icons-material/PersonAdd";
import AddPhotoAlternateIcon from "@mui/icons-material/AddPhotoAlternate";
import { useState, useEffect } from "react";
import { BsFillBarChartLineFill } from "react-icons/bs";
import axios from "axios";
import { isOptionGroup } from "@mui/base";
import YouCantDelete from "../Team/YouCantDelete";
import Popup from "../Popup/Popup";


function AddEvaluation({ closeHandler, id, fname, lname }) {


  const [rating, setrating] = useState(2);
  const [feedback, setFeedback] = useState([]);
  const [showcant, setShowCant] = useState(false);
  const [kpi, setKpi] = useState([]);
  const [kpiid, setKpiId] = useState(1);
  const [check,setCheck]=useState('')
  const [loading, setLoadingkpi] = useState(false);
  const showPopupCant = () => {
    setShowCant(true);

  };
  const closeHandlerCant = () => {
    setShowCant(false);
    document.body.style.close = "auto";
  };
  useEffect(() => {
    getkpi();
  }, []);
  
  const checkDate = async (e) => {
   e.preventDefault()
    try {
      const response = await axios.get(`${process.env.REACT_APP_ERP_URL}/api/checkdate?kpiid=${kpiid}&employeeid=${id}`);
      const { data: check } = response;
      setCheck(check)
      console.log(check)
     if(check.date!=""){
      showPopupCant()
     }else{
     onSubmitHandler()
     }
    } catch (error) {
      console.log(error.message);
    }
    
  };
  const getkpi = () => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/kpi`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((kpi) => {
        setKpi(kpi);
        setLoadingkpi(false)
      });
  };

  const onSubmitHandler = (e) => {
   
    var todayDate = new Date().toISOString().slice(0, 10);
    
    const formData = new FormData();
    formData.append("employee_id", id);
    formData.append("kpi_id", kpiid);
    formData.append("rating", rating);
    formData.append("edate", todayDate);
    formData.append("feedback", feedback);

    axios
      .post(`${process.env.REACT_APP_ERP_URL}/api/evaluation`, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        console.log("SuccesssetSysidfuly Sent!")
        window.location.reload(false)
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const onChangeRating = (e) => {
    setrating(e.target.value);
  };



  const rating_arr = Array.from({ length: 10 }, (_, i) => i + 1);
if(loading) return "loading..."
  return (
    <div className="Add-Box_evaluatoin">
      <span className="close" onClick={closeHandler}>
        &times;
      </span>
      <h1 className="name_rating">
        <BsFillBarChartLineFill /> Evaluation Form
      </h1>
      <hr></hr>
      <form>
        <h3 className="name_rating">
          Employee: {fname} {lname}
        </h3>

        
        <select name="kpi" className="input-Box box select" value={kpiid} onChange={(e)=>setKpiId(e.target.value)}>
              {kpi.map((info,index) =>
                <option key={index} value={info.id}>{info.kpiname}</option>)
              }
            </select>

    

        <p>KPI Rating:
        </p>

<div class="slidecontainer">
  <input type="range" min="1" max="10" value={rating}  onChange={(e) => {
            setrating(e.target.value);
          }}class="slider" id="myRange"/>
  <p>Value: <span>{rating}</span></p>
</div>

        <textarea
          className="feedback_rating"
          name="feedback"
          id=""
          cols="30"
          rows="5"
          value={feedback}
          onChange={(e) => {
            setFeedback(e.target.value);
          }} required
        />

        <button
          className="button_rating_submit"
          role="button"
          onClick={checkDate}
        >
          {" "}
          Submit
        </button>
      </form>
      <Popup
        show={showcant}
        overlay={false}
        content={
          <YouCantDelete
            closeHandler={closeHandlerCant}
            content="The Evaluation on this KPI is already done today."

          />
        }
      />
    </div>
  );
}

export default AddEvaluation;
