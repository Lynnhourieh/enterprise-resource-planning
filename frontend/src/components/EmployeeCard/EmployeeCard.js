import React from "react";
import "./EmployeeCard.css";
import {useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import CircleIcon from "@mui/icons-material/Circle";
function TeamCard ({id,fname,image,lname,isActive,phone, email}){
    const [data, setData] = useState([isActive]);
    const navigate = useNavigate();
    const Profile=()=>{
        navigate('/profile',{ state: { id: id } });
      }
    return(<>
    

<div class="first hero"onClick={Profile}>
  <img class="hero-profile-img"  src={`${process.env.REACT_APP_ERP_URL}/images/${image}`} alt=""/>
  <div class="hero-description-bk"></div>
 
  <div class="hero-description">
  
  </div>
  <div  class="hero-date">
     <div>{fname} {lname}  {data == "active" ? (
                  <CircleIcon sx={{ color: "green" }}/>
                ) : (
                  <CircleIcon style={{ color: "red" }} />
                )}</div>
   
     
  </div>
 
</div>

 </>
     
  );}
export default TeamCard;
