import React from "react";
import { useState, useEffect,PureComponent, useCallback } from "react";
import  "./dashboard.css"
import Boxes from "../Boxes/Boxes"
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,   Radar,
  RadarChart,
  PolarGrid,
  PolarAngleAxis,
  PolarRadiusAxis, PieChart, Pie, Cell, Area, AreaChart,   Sector} from 'recharts';
  import Popup from "../Popup/Popup";
import EmployeeReport from "../Employee Report/EmployeeReport";

  const renderActiveShape = (props) => {
    const RADIAN = Math.PI / 180;
    const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';
  
    return (
      <g>
        <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
          {payload.name}
        </text>
        <Sector
          cx={cx}
          cy={cy}
          innerRadius={innerRadius}
          outerRadius={outerRadius}
          startAngle={startAngle}
          endAngle={endAngle}
          fill={fill}
        />
        <Sector
          cx={cx}
          cy={cy}
          startAngle={startAngle}
          endAngle={endAngle}
          innerRadius={outerRadius + 6}
          outerRadius={outerRadius + 10}
          fill={fill}
        />
        <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
        <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`${value} Employees`}</text>
        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
          {`(Rate ${(percent * 100).toFixed(2)}%)`}
        </text>
      </g>
    );
  };
  
  const renderActiveShapeteam = (props) => {
    const RADIAN = Math.PI / 180;
    const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';
  
    return (
      <g>
        <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
          {payload.name}
        </text>
        <Sector
          cx={cx}
          cy={cy}
          innerRadius={innerRadius}
          outerRadius={outerRadius}
          startAngle={startAngle}
          endAngle={endAngle}
          fill={fill}
        />
        <Sector
          cx={cx}
          cy={cy}
          startAngle={startAngle}
          endAngle={endAngle}
          innerRadius={outerRadius + 6}
          outerRadius={outerRadius + 10}
          fill={fill}
        />
        <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
        <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#333">{`${value} Employees`}</text>
        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
          {`(Rate ${(percent * 100).toFixed(2)}%)`}
        </text>
      </g>
    );
  };
  

  const renderActiveShapeactive = (props) => {
    const RADIAN = Math.PI / 180;
    const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';
  
    return (
      <g>
        <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>
          {payload.name}
        </text>
        <Sector
          cx={cx}
          cy={cy}
          innerRadius={innerRadius}
          outerRadius={outerRadius}
          startAngle={startAngle}
          endAngle={endAngle}
          fill={fill}
        />
        <Sector
          cx={cx}
          cy={cy}
          startAngle={startAngle}
          endAngle={endAngle}
          innerRadius={outerRadius + 6}
          outerRadius={outerRadius + 10}
          fill={fill}
        />
        <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
        <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor}  fill="#333" >{`${value} Employees`}</text>
        <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#999">
          {`(Rate ${(percent * 100).toFixed(2)}%)`}
        </text>
      </g>
    );
  };
  

const Dashboard=({payload})=> {




  const [loading, setLoading] = useState(true);

  const [employee, setEmployee] = useState([]);
  const [result, setResult] = useState([]);
  const [result_team_arr, setResult_team] = useState([]);
  const [result_active_arr, setResult_active] = useState([]);
  const [employeeid, setEmployeeId] = useState([]);
  const [datakpi, setDatakpi] = useState("");
  const [single, setSingle] = useState("");
  const [gender,setGender]=useState('')
  const [piechart,setPiechart]=useState('')
  const [piechartactive,setPiechartactive]=useState('')
  const [data, setData] = useState([])
  const [show, setShow] = useState(false);
  const showPopup = () => {
    setShow(true);
    
    fetch(`${process.env.REACT_APP_ERP_URL}/api/report?employee_id=${employeeid}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
     
        setData(data);
        
      });

  };
  const closeHandler=()=>{
    setShow(false)
  }
  useEffect(() => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/employee`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((employee) => {
        setLoading(false);
        setEmployee(employee);
        console.log(employee)
      });
     Gender()
    piechartTeam()
    Piechartactive()
     
  }, []);

  const Gender=()=>{
    fetch(`${process.env.REACT_APP_ERP_URL}/api/gender`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((gender) => {
       
        setGender(gender);

        const result = Object.entries(gender).map(([name, value]) => ({name, value}))
        
        setResult(result);
       
      });
  }

  const piechartTeam=()=>{
    fetch(`${process.env.REACT_APP_ERP_URL}/api/piechartteam`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((piechart) => {
       
        setPiechart(piechart);

        // const result_team = Object.entries(piechart).map(([value]) => ({value}))
        const result_team_arr = piechart.teams;

        // const result_team = Object.entries(piechart).map(([name, value]) => ({name, value}))

        setResult_team(result_team_arr);
        console.log(result_team_arr);
        // console.log(result_team_arr,'team_arrrrrrrrrrr');
     
      });
  }


  const Piechartactive=()=>{
    fetch(`${process.env.REACT_APP_ERP_URL}/api/piechartactive`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((piechartactive) => {
       
        setPiechartactive(piechartactive);

        // const result_team = Object.entries(piechart).map(([value]) => ({value}))
        // const result_active_arr = piechartactive;

        // const result_team = Object.entries(piechart).map(([name, value]) => ({name, value}))

        setPiechartactive(piechartactive);
        const result_active_arr = Object.entries(piechartactive).map(([name, value]) => ({name, value}))

        setResult_active(result_active_arr);
         
      });
  }
  const onSubmitHandler=(id)=>{
  
    fetch(`${process.env.REACT_APP_ERP_URL}/api/chart?employee_id=${id}`, {
      
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        } throw reponse;
      }).then(datakpi => {
        setDatakpi(datakpi)
        setEmployeeId(datakpi[0].employee_id)
       console.log(datakpi)
      },[])
      
  };
 const getKpi=(name)=>{
  
    fetch(`${process.env.REACT_APP_ERP_URL}/api/evaluations?id=${name}&employeeid=${employeeid}`, {
      
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        } throw reponse;
      }).then(single => {
        setSingle(single)
       
   
      },[])
    
  };
  

  const [activeIndex, setActiveIndex] = useState(0);
  const onPieEnter = useCallback(
    (_, index) => {
      setActiveIndex(index);
    },
    [setActiveIndex]
  );

  const [activeIndext, setActiveIndext] = useState(0);
  const onPieEntert = useCallback(
    (_, index) => {
      setActiveIndext(index);
    },
    [setActiveIndext]
  );

  const [activeIndexa, setActiveIndexa] = useState(0);
  const onPieEntera = useCallback(
    (_, index) => {
      setActiveIndexa(index);
    },
    [setActiveIndexa]
  );


  class CustomizedAxisTick extends PureComponent {
    render() {
      const { x, y, stroke, payload } = this.props;
     const newdate = payload.value;
     const date1= newdate.toString().slice(0,7);
      console.log(date1)



      return (
        <g transform={`translate(${x},${y})`}>
          <text x={0} y={0} dy={16} dx={10}   textAnchor="end" fill="#666" >
            {date1}
          </text>
        </g>
      );
    }
  }

  const onChangeemloyee = (id) => {
    setEmployeeId(id);
  };
  return (


<>
    
    <div className='dashboard_main'>
       <Boxes />
       <div className="gender_section">
    

     <PieChart width={400} height={400}>
          <Pie
            activeIndex={activeIndext}
            activeShape={renderActiveShapeteam}
            data={result_team_arr}
            cx="50%"
            cy="50%"
            innerRadius={60}
            outerRadius={80}
            fill="#8884d8"
            dataKey="value"
            onMouseEnter={onPieEntert}
          />
        </PieChart>

        <PieChart width={400} height={400}>
          <Pie
            activeIndex={activeIndex}
            activeShape={renderActiveShape}
            data={result}
            cx="50%"
            cy="50%"
            innerRadius={60}
            outerRadius={80}
            fill="#8884d8"
            dataKey="value"
            onMouseEnter={onPieEnter}
          />
        </PieChart>

        <PieChart width={400} height={400}>
          <Pie
            activeIndex={activeIndexa}
            activeShape={renderActiveShapeactive}
            data={result_active_arr}
            cx="50%"
            cy="50%"
            innerRadius={60}
            outerRadius={80}
            fill="#8884d8"
            dataKey="value"
            onMouseEnter={onPieEntera}
          />
        </PieChart>

        
    </div>
    <div className="kpi_report_header">
    <h3>kPI Report</h3>
 <select name="employee" className="input_employee_chart" value={employeeid} onChange={(e)=>onChangeemloyee(e.target.value)} onClick={(e)=>onSubmitHandler(e.target.value)}>
              
              {
              employee.map((info,index) =>
                <option key={index} value={info.id}>{info.fname} {info.mname} {info.lname}</option>)
              }
            </select>
            <button type="submit" onClick={showPopup}>Work History</button>



  </div>
      
  {/* <LineChart width={500} height={300} data={data}>
    <XAxis dataKey="name"/>
    <YAxis/>
    <CartesianGrid stroke="#eee" strokeDasharray="1 10"/>
    <Line type="monotone" dataKey="uv" stroke="#8884d8" />
   
  </LineChart> */}

  <div className="chart_section"> 

   <div className='kpi_section'>
     
     
{datakpi!=""?
    <RadarChart
 
      cx={250}
      cy={250}
      outerRadius={150}
      width={500}
      height={500}
      data={datakpi}
      
   
    >
      <PolarGrid />
      <PolarAngleAxis dataKey={"kpiname"}  onClick={(e)=>getKpi(e.value)}/>
     
      {/* <PolarRadiusAxis domain={[0, 10]}  angle={45}  stroke="#000000" > */}
      <Radar 
        name="kpiname"
        dataKey={"rating"}
        stroke="#8884d8"
        fill="#8884d8"
        fillOpacity={0.6}
        nameKey="rating"
        label={{ value: 'pv of page', angle: 0, position: 'inside Left' }}
        
       
        >
          <label/>
           </Radar>
      
    </RadarChart>
:""}
    </div>

    <div className="graph_chart">
    {single!=""?
    <AreaChart
      width={500}
      height={400}
      data={single}
      margin={{
        top: 10,
        right: 30,
        left: 0,
        bottom: 0
      }}
      
    >
      <CartesianGrid strokeDasharray="10 10" />
      <XAxis dataKey={"edate"} tick={<CustomizedAxisTick/>}/>
      <YAxis type="number" domain={[0, 10]}  tickCount={10}/>
      <Tooltip />
      <Area type="monotone" dataKey="rating" stroke="#8884d8" fill="#8884d8" />
    </AreaChart>
   :""}

    </div>
 
    </div>
  
    <Popup
          show={show}
          overlay={true}
          content={
            <EmployeeReport
              data={data}
              closeHandler={closeHandler}
            />
          }
        />
  </div>


   </> 
  )
}
export default Dashboard