import React from 'react'
import YouCantDelete from '../Team/YouCantDelete';
import { useEffect, useState } from "react"
import axios from 'axios';
import Popup from '../Popup/Popup';
function AssginEmployeeRoles({ closeHandler, id, pname }) {

    const [project, setProject] = useState('')
    const [projectid, setProjectId] = useState('')
    const [loading, setLoading] = useState(true)
    const [employeeid, setEmployeeId] = useState([])
    const [roles, setRoles] = useState([])
    const [rolesid, setRolesId] = useState(1)
    const [role, setRole] = useState([])
    const [showcant, setShowCant] = useState(false);
   const [dataemployee,setDataEmployee]=useState([])
  
    const showPopupCant = () => {
        setShowCant(true);
    
      };
      const closeHandlerCant = () => {
        setShowCant(false);
        document.body.style.close = "auto";
      };
      const getEmployees=async (e)=>{
        
        await fetch(`${process.env.REACT_APP_ERP_URL}/api/getemployeeroles?project_id=${id}`, {
          
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(reponse => {
            if (reponse.ok) {
              return reponse.json();
            
            } 
          }).then(dataemployee => {
            setDataEmployee(dataemployee)
            setEmployeeId(dataemployee.message[0].id)
            setLoading(false)
           
          },[])
        
      };
      
    const getroles = async () => {

        await fetch(`${process.env.REACT_APP_ERP_URL}/api/roles`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        })
            .then((reponse) => {
                if (reponse.ok) {
                    return reponse.json();
                }
                throw reponse;
            })
            .then((roles) => {
               setLoading(false)
                setRoles(roles);
            });
    }
   
//     const checkRole =  async(e) => {
//         // e.preventDefault()
        
//             await fetch(`${process.env.REACT_APP_ERP_URL}/api/assgin?project_id=${id}`, {

//                 headers: {
//                   "Content-Type": "application/json"
//                 }
//               })
//                 .then(reponse => {
//                   if (reponse.ok) {
//                     return reponse.json();
//                   } throw reponse;
//                 }).then(role => {
//                   setRole(role)
//                   setEmployeeId(role[0].employee_id)
//                   setLoading(false)
//                 }, [])
    
// }
    useEffect(() => {
        getEmployees()
        getroles()
        // checkRole()
    }, [])
   
    const onSubmitHandler =  (e) => {
       e.preventDefault()
        let todayDate = new Date().toISOString().slice(0, 10);
        const formData = new FormData();
        formData.append("employee_id", employeeid)
        formData.append("project_id", id)
        formData.append("role_id", rolesid)
        formData.append("startdate", todayDate)
         axios
            .post(`${process.env.REACT_APP_ERP_URL}/api/assgin`, formData, {
                headers: { "Content-Type": "multipart/form-data" },
            })
            .then((res) => {
                console.log("SuccesssetSysidfuly Sent!")
                window.location.reload(false)


            })
            .catch((err) => {
                console.log(err);

            });
    };

 if(loading) return "loading..."
    return (
        <div className="login-box">

            <span className="close" onClick={closeHandler}>
                &times;
            </span>
            <h1>Assgin Team To Project</h1>
            <hr></hr>
            <form>
                <div className="user-box">
                    <input type="text" name="" value={pname}>

                    </input>
                    <label>Project name</label>
                </div>



                <div className="select-projects">
                    <div>
                        <label>Employee</label>
                        <select name="" value={employeeid} onChange={(e) => {
                            setEmployeeId(e.target.value);
                                                 }} >
                            {
                                dataemployee?.message?.map((info) =>
                                    <option value={info.id}>{info.fname} {info.mname} {info.lname}</option>)
                            }
                        </select>

                        <label>Role</label>
                        <select name="" value={rolesid} onChange={(e) => {
                            setRolesId(e.target.value);
                        }}>
                            {
                                roles?.map((info) =>
                                    <option  value={info.id}>{info.rname}</option>)
                            }
                        </select>

                    </div>
                    <div>
                        <button class="button-6" role="button" onClick={onSubmitHandler}>Assgin</button>

                    </div>
                </div>



            </form>
            <Popup
        show={showcant}
        overlay={false}
        content={
          <YouCantDelete
            closeHandler={closeHandlerCant}
            content="Already assgnied to this role in this project."

          />
        }
      />
        </div>
    )
}

export default AssginEmployeeRoles
