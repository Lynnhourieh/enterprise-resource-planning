import React from 'react'
import DeleteIcon from '@mui/icons-material/Delete';
import axios from "axios"
import { BsWindowSidebar } from 'react-icons/bs';
import { useNavigate } from "react-router-dom"
function TeamProjectCards({tname,id,projectid}) {
    const navigate = useNavigate();
    const teamDetails = () => {
      navigate('/teamdetails', { state: { id: id,tname:tname } })
  
    }


    const onSubmitHandler = (e) => {
        
        axios
          .delete(`${process.env.REACT_APP_ERP_URL}/api/deleteassgin?team_id=${id}&project_id=${projectid}`,{
            headers: { "Content-Type": "multipart/form-data" },
          })
          .then((res) => {
            console.log("SuccesssetSysidfuly Sent!")
            window.location.reload(false)
            
            
          })
          .catch((err) => {
            console.log(err);
           
          },[]);
      };
  return (
    <div class="container">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">

          <div class="our-team">

            <div class="picture">

              <img class="img-fluid" src="https://blog.masterofproject.com/wp-content/uploads/2017/03/Closing-Process-Group-3.jpg" />
            </div>
            <div class="team-content" >
              <h3 class="nameemployee" onClick={teamDetails}>{tname}</h3>
            </div>
 <div class="delete-button" >
                  <button className="delete-team" role="button" onClick={onSubmitHandler}><DeleteIcon /></button>
                </div>
          </div>

        </div>


      </div>
  )
}

export default TeamProjectCards
