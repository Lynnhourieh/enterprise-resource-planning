import React from "react";
import CircleIcon from "@mui/icons-material/Circle";
import { useState, useEffect } from "react";
import {useNavigate} from "react-router-dom";
function EmployeeProjectCards ({id,projectid,fname,image,lname,isActive}){
    const [data, setData] = useState([isActive]);
    const [dataemployee, setDataEmployee] = useState([]);
    const [rname, setRname] = useState([]);
    const [roleid, setRoleId] = useState([]);
    const [classs, setClasss] = useState(false);
    const navigate=useNavigate()
    
    const Profile=()=>{
        navigate("/profile",{state:{id:id}})
    }
    const rolsdetails=()=>{
      navigate("/roledetails",{state:{id:roleid}})
  }
    const getRole= async(e)=>{
       
         await fetch(`${process.env.REACT_APP_ERP_URL}/api/assgin?employee_id=${id}&project_id=${projectid}`, {
          
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(reponse => {
            if (reponse.ok) {
              return reponse.json();
            
            } 
          }).then(dataemployee => {
            setDataEmployee(dataemployee)
            setRname(dataemployee[0].rname)
            setRoleId(dataemployee[0].id)
          },[])
      };
    useEffect(()=>{
getRole()
if(rname==""){
  setClasss(false)
}else{
  setClasss(true)
}
    },[roleid])
   
    return(
        <div class="container">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
          <div class="our-team">
            <div class="picture">
              <img src={`${process.env.REACT_APP_ERP_URL}/images/${image}`} />
            </div>
            <div class="team-content">
              <h3 class="nameemployee" onClick={Profile}>
                {fname} {lname}{" "}
                {data == "active" ? (
                  <CircleIcon sx={{ color: "green" }} />
                ) : (
                  <CircleIcon style={{ color: "red" }} />
                )}
              </h3>
  {classs?
              <h5 class="titleemployee" onClick={rolsdetails}>Role:{rname}</h5>
 :<h5 class="titleemployee">No Role Assgined Yet </h5>}
              <h5 class="titleemployee"></h5>
            </div>
          </div>
        </div>
        </div>
   )
}
export default EmployeeProjectCards;