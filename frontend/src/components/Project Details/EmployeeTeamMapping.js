import React, { useEffect } from 'react'
import EmployeeProjectCards from './EmployeeProjectCard'
import {useState} from "react"
import TeamProjectCards from './TeamProjectCards'
function EmployeeTeamMapping({getEmployees,dataemployee,id}) {

 useEffect(()=>{
getEmployees()

 },[])

  return (
    <div className="team-cards">

{
(dataemployee?.message)?
       dataemployee?.message?.map((datas,index) => 
       <TeamProjectCards key={index} id={datas.id} projectid={datas.project_id} tname={datas.tname} /> 
     )
       :
       dataemployee?.map((datas,index) => 
       <EmployeeProjectCards key={index} id={datas.id} projectid={id} fname={datas.fname} lname={datas.lname} image={datas.image} isActive={datas.isActive}/> 
     )
       } 
        </div>
  )
}

export default EmployeeTeamMapping