import React, { useEffect } from 'react'

import EmployeeTeamMapping from './EmployeeTeamMapping'
import { useLocation, useNavigate } from "react-router-dom";
import { useState } from "react"
import Popup from '../Popup/Popup';

import AssginEmployeeRoles from "./AssginEmployeeRoles"
import AssginTeams from "../Projects/AssginTeams"
import { listItemSecondaryActionClasses } from '@mui/material';
function ProjectDetails() {
  const [dataemployee, setDataEmployee] = useState([])
  const [showassgin, setShowAssgin] = useState(false);
  const [showassginemployee, setShowAssginEmployee] = useState(false);
  const [loading,setloading]=useState(true)
  
  const navigate = useNavigate();
  const location = useLocation();

  const closeHandler = () => {
    setShowAssginEmployee(false)
    setShowAssgin(false)
    document.body.style.close = "auto";
  };
  const showAssgin = () => {
    setShowAssgin(true);

  };
  const showAssginEmployee = () => {
    setShowAssginEmployee(true);

  };
  const getEmployees = async (e) => {

    await fetch(`${process.env.REACT_APP_ERP_URL}/api/getemployeeproject?project_id=${location.state.id}`, {

      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();

        }
      }).then(dataemployee => {
        setDataEmployee(dataemployee)
       
      }, [])
  };
  const getTeams = async (e) => {

    await fetch(`${process.env.REACT_APP_ERP_URL}/api/getteamproject?project_id=${location.state.id}`, {

      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();

        }
      }).then(dataemployee => {
        setDataEmployee(dataemployee)
        
        console.log(dataemployee)
      }, [])
  };
  // useEffect(() => {
  //   if (location.state === null) {
  //     navigate('/projects')
  //   }

  // }, [])
// if(loading) return "loading..."
  return (
    <div className='wraping-page'>
      <h1 className='projectheader'>
        

        {location.state.pname}

      </h1>
      <div className='button-details'>

        <div>
          <button className="button-68" type="submit" onClick={getEmployees}>Employees</button>
          <button className="button-68" type="submit" onClick={getTeams}>Teams</button>
        </div>
        <button className="button-68" type="submit" onClick={showAssgin}>Assgin Teams</button>
        <button className="button-68" type="submit" onClick={showAssginEmployee}>Assgin Roles</button>
      </div>
      <div>

        <EmployeeTeamMapping id={location.state.id} getEmployees={getEmployees} dataemployee={dataemployee} />

      </div>

      <Popup
        show={showassgin}
        overlay={true}
        content={
          <AssginTeams closeHandler={closeHandler}
            id={location.state.id}
            pname={location.state.pname}
          />
        }
      />

      <Popup
        show={showassginemployee}
        overlay={true}
        content={
          <AssginEmployeeRoles closeHandler={closeHandler}
            id={location.state.id}
            pname={location.state.pname}


          />
        }
      />

    </div>
  )
}

export default ProjectDetails