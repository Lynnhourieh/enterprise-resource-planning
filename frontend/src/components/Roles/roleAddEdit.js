
import React from "react";
import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import axios from "axios";
function RoleAddEdit({closeHandler,id,Rname,setRname}){
  // const [data, setData] = useState([]);
 const[rolename,setRnames]=useState("")
  const[data,setData]=useState('');
  const onSubmitHandler = () => {
    const formData = new FormData();
    formData.append("Rname", rolename);
    axios
      .post(`${process.env.REACT_APP_ERP_URL}/api/roles`, formData, {
       
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => console.log("Successfuly Sent!"))
      .catch((err) => {
        console.log(err);
      });
  };
 
  

  const onUpdateRoles = (e) => {
   e.preventDefault();
   
    
      axios
        .put(`${process.env.REACT_APP_ERP_URL}/api/roles/${id}?Rname=${Rname}`, {

          headers: { 'Content-Type': 'multipart/form-data' },

        })
        .then((res) =>{ console.log('Successfuly Sent!')
      window.location.reload(false)})
        .catch((err) => {

          console.log(err);
        });
  
  }
if(!id){
    return(
    <div className="login-box">
    
       <span className="close" onClick={closeHandler}>
          &times;
        </span>
    <h1>Add New Role</h1>
    <hr></hr>
    <form>
      <div class="user-box">
        <input type="text" name="" value={rolename} onChange={(e) => {
                setRnames(e.target.value);
              }}
              required/>
        <label>Role name</label>
      </div>
     
      <button class="button-6" role="button" onClick={onSubmitHandler}>Add</button>
      
        
      
    </form>
  </div>
  )}else if(id){
    return(
      <div className="login-box">
      
         <span className="close" onClick={closeHandler}>
            &times;
          </span>
      <h1>Add New Role</h1>
      <hr></hr>
      <form>
        <div class="user-box">
          <input type="text" name="" value={Rname} onChange={(e) => {
                  setRname(e.target.value);
                }}
                required/>
          <label>Role name</label>
        </div>
       
        <button class="button-6" role="button" onClick={onUpdateRoles}>EDIT</button>
        
          
        
      </form>
    </div>
    )
  }
}

export default RoleAddEdit