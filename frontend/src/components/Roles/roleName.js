import "./roles.css";
import React from "react";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Popup from "../Popup/Popup";
import { useEffect, useState } from "react";
import RoleAddEdit from "./roleAddEdit";
import DeleteConfermation from "../Team/DeleteConfermation";
import YouCantDelete from "../Team/YouCantDelete";

import axios from "axios";
import { useNavigate } from "react-router-dom"
function RoleName({ Rname, id, showPopupEdit }) {

  const [showdelete, setShowDelete] = useState(false);
  const [showcant, setShowCant] = useState(false);
  const [employee, setEmployee] = useState([]);
  const [data, setData] = useState('');

  const navigate = useNavigate();
  const roleDetails = () => {
    navigate('/roledetails', { state: { id: id, Rname: Rname } })

  }

  const showPopupDelete = () => {
    setShowDelete(true);

  };

  const showPopupCant = () => {
    setShowCant(true);

  };
  const closeHandlerCant = () => {
    setShowCant(false);
    document.body.style.close = "auto";
  };

  const closeHandlerDelete = () => {
    setShowDelete(false);
    document.body.style.close = "auto";
  };
  const onSubmitHandler = (e) => {

    fetch(`${process.env.REACT_APP_ERP_URL}/api/checkroles?id=${id}`, {

      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        } throw reponse;
      }).then(data => {
        setData(data)
        if (data.status != "") {
          showPopupCant();

        } else {
          return showPopupDelete();
        }
      }, [])
  };
  const getemployees = async () => {

    try {
      const response = await axios.get(`${process.env.REACT_APP_ERP_URL}/api/countemployeerole?id=${id}`);
      const { data: employee } = response;
      setEmployee(employee)


    } catch (error) {
      console.log(error.message);
    }

  };

  const onDeleteTeam = (e) => {

    axios
      .delete(`${process.env.REACT_APP_ERP_URL}/api/roles/${id}`, {

        headers: { 'Content-Type': 'multipart/form-data' },


      })
      .then((res) => {
        console.log('Successfuly Sent!')
        window.location.reload(false)
      })
      .catch((err) => {

        console.log(err);
      });
  };

  useEffect(() => {
    getemployees()
  }, [])

  return (
    <div class="cards-list">

      <div class="card 1" >


        <div class="card_title title-white">

          <div class="card_image"> </div>
          <p onClick={roleDetails}>{Rname}</p>
          <h4 class="titleemployee">Employees:{employee}

          </h4>
          <div class="edit-delete-button" >

            <div className="line" onClick={onSubmitHandler} >
              <DeleteIcon sx={{ color: "black", fontSize: 20 }} /></div>


            <div class="line" onClick={() => showPopupEdit(id, Rname)} >
              <EditIcon sx={{ color: "black", fontSize: 20 }} />
            </div>
          </div>
        </div>

      </div>

      {/* <div class="card 2">
        <div class="card_image">
          <img src="https://cdn.blackmilkclothing.com/media/wysiwyg/Wallpapers/PhoneWallpapers_FloralCoral.jpg" />
        </div>
        <div class="card_title title-white">
          <p>{Rname}</p>
        </div>
      </div> */}

      {/* <div class="card 3">
        <div class="card_image">
          <img src="https://media.giphy.com/media/10SvWCbt1ytWCc/giphy.gif" />
        </div>
        <div class="card_title">
          <p>Card Title</p>
        </div>
      </div> */}

      {/* <div class="card 4">
        <div class="card_image">
          <img src="https://media.giphy.com/media/LwIyvaNcnzsD6/giphy.gif" />
        </div>
        <div class="card_title title-black">
          <p>Card Title</p>
        </div> */}
      {/* </div> */}


      {/* // <div class="container" >
    //   <div class="container">




    //     <div class="col-12 col-sm-6 col-md-4 col-lg-3">

    //       <div class="our-team">

    //         <div class="team-content" >

    //           <h4 class="nameemployee" onClick={roleDetails}>{Rname}</h4>
    //           <h4 class="titleemployee">Employees:{employee} </h4>
    //           <h5 class="titleemployee">
    //             <div class="delete-button" >
    //               <DeleteIcon sx={{ color: '#57bee6' }} onClick={onSubmitHandler} />
    //             </div>
    //             <div class="col col-4 ">
    //               <EditIcon sx={{ color: '#57bee6' }} onClick={() => showPopupEdit(id, Rname)} />
    //             </div>

    //           </h5>

    //         </div>

    //       </div>

    //     </div>


    //   </div> */}


      <Popup
        show={showdelete}
        overlay={true}
        content={
          <DeleteConfermation
            closeHandlerDelete={closeHandlerDelete}
            onDeleteTeam={onDeleteTeam}
            content="Are You Sure You Want To Delete This Team."




          />
        }
      />

      <Popup
        show={showcant}
        overlay={true}
        content={
          <YouCantDelete
            closeHandler={closeHandlerCant}
            content="This Team Has Employees You Can't Delete It."

          />
        }
      />


    </div>

  );
}

export default RoleName;
