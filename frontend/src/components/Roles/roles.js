import "./roles.css";
import RoleName from "./roleName";
import { useState, useEffect } from "react";
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import Search from "../Search/Search";
import RoleAddEdit from "./roleAddEdit";
import Popup from "../Popup/Popup";
import { StyledEngineProvider } from "@mui/material";
function Role() {
  const [data, setData] = useState([]);
  const [loading, setloading] = useState(true);
  const [show, setShow] = useState(false);
  const [showedit, setShowEdit] = useState(false);
  const [id,setId]=useState('')
  const[Rname,setRname]=useState('')
  const showPopup = () => {
    setShow(true);
  };
  const showPopupEdit = (id,Rname) => {
    setShowEdit(true);
    setId(id)
    setRname(Rname)
  };
  const closeHandler = () => {
    setShow(false);
    setShowEdit(false)
    document.body.style.close = "auto";
  };

  useEffect(() => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/roles`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setloading(false);
        setData(data);
      });
  }, []);
  const onSearchHandler = async (e,search) => {
    
    e.preventDefault();
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/searchrole?rname=${search}`, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        }
      }).then(data => {
        setData(data)
      })
  }
  const onSearchRoles = async (e,search) => {
    
    e.preventDefault();
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/searchrole?rname=${search}`, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        }
      }).then(data => {
        setData(data)
      })
  };
  if (loading) return "loading...";

  return( 
<>
    <div class="container">
   
   
        
        <div class="projectheader"><b>Roles</b></div>
        <Search onSearchHandler={onSearchRoles}/>
        <div className="line">
        <GroupAddIcon onClick={showPopup} sx={{fontSize:30}}/>
        </div>
  </div>
  <div className="team-cards">
    { 
           data.map((datas) => 
            <RoleName key={datas.id} id={datas.id} Rname={datas.rname}  showPopupEdit={showPopupEdit}/> 
          )
        } 
        </div>
       <Popup
          show={show}
          title=""
          content={
            <RoleAddEdit closeHandler={closeHandler}
            />
          }
        />
         <Popup
      show={showedit}
      content={
        <RoleAddEdit 
         closeHandler={closeHandler}
         id={id}
         Rname={Rname}
        setRname={setRname}
        />
      }
    />
    
  </>);
}

export default Role;
