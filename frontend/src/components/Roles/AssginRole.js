import React from 'react'
import axios from "axios"
import {useEffect,useState} from "react"
import Popup from '../Popup/Popup';
import YouCantDelete from './YouCantDelete';
function AssginProjects({ closeHandler,id,rname }) {
const [rolesandprojects,setrolesAndProjects]=useState([]);
const [project,setProject]=useState(1);
const [projectid,setProjectId]=useState([]);
const [loading,setLoading]=useState(true)
const [showcant, setShowCant] = useState(false);
const showPopupCant = () => {
  setShowCant(true);
};
const closeHandlerCant = () => {
  setShowCant(false);
  document.body.style.close = "auto";
};
    const getprojects = async () => {
      
      try {
        const response = await axios.get(`${process.env.REACT_APP_ERP_URL}/api/getprojects`);
        const { data: project } = response;
        setProject(project)
        setProjectId(project[0].id)
        setLoading(false)
        
      } catch (error) {
        console.log(error.message);
      }
      
    };
      useEffect(() => {
        getprojects()
      }, [])
     
      const onSubmitHandler = (e) => {
        const formData = new FormData();
        formData.append("team_id", id)
        formData.append("project_id", projectid)
        axios
          .post(`${process.env.REACT_APP_ERP_URL}/api/assginprojects`,formData,{
            headers: { "Content-Type": "multipart/form-data" },
          })
          .then((res) => {
            console.log("SuccesssetSysidfuly Sent!")
            window.location.reload(false)
            
            
          })
          .catch((err) => {
            console.log(err);
           
          });
      };
     
      const checkProjectsAndTeams=(e)=>{
    e.preventDefault()
        fetch(`${process.env.REACT_APP_ERP_URL}/api/assginprojects?team_id=${id}&project_id=${projectid}`, {
          
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(reponse => {
            if (reponse.ok) {
              return reponse.json();
            } throw reponse;
          }).then(rolesandprojects => {
            setrolesAndProjects(rolesandprojects)
           
            if(rolesandprojects.status!=""){
              
                    showPopupCant()
                    
                   }else{
                     onSubmitHandler()
                   }
          },[closeHandler])
      };
     
   
     if(loading)return "loading.."
    return (
        <div className="login-box">

            <span className="close" onClick={closeHandler}>
                &times;
            </span>
            <h1>Assgin Team To Project</h1>
            <hr></hr>
            <form>
                <div className="user-box">
                    <input type="text" name=""
                           value={rname}
                        required />
                    <label>Team name</label>
                </div>


                <div className="select-projects">

                    <label>Projects</label>

                    <select name="project_id" value={projectid} onChange={(e) => {
                setProjectId(e.target.value);
              }} >
                        {
                        project.map((info) =>
                <option key={info.id} value={info.id}>{info.pname}</option>)
              }
                    </select>

                </div>

                <div>
                    <button class="button-6" role="button" onClick={checkProjectsAndTeams}>Assgin</button>

                </div>

            </form>
            <Popup
          show={showcant}
          setShow={setShowCant}
          content={
            <YouCantDelete 
             closeHandler={closeHandlerCant}
             content="This Project Already Assgined To This Team."
             id={id}
            />
          }
         
        />
        </div>
         
    )
}

export default AssginProjects