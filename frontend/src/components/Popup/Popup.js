import { useEffect } from "react";
import "./Popup.css";

const Popup = ({ show, content,overlay }) => {
  useEffect(() => {
    if (show) {
      document.body.style.overflow = "hidden";
    } else {
      document.body.style.overflow = "auto";
    }
  }, [show]);

  
  return (
    <div
      style={{
        display: show ? "block" : "none",
        opacity: show ? "1" : "0",
      }}
      className={`overlay ${overlay ?  "background-overlay" : ""}`}
    >
      <div className="popup" >
        <div className="content" >{content }</div>
      </div>
   
    </div>
  );
};

export default Popup;
