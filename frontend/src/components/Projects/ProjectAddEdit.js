import React from 'react'
import axios from "axios"
import { useState, useEffect } from "react";
import { RiEyeCloseFill } from 'react-icons/ri';
function ProjectAddEdit({ closeHandler, id,pname,discription,startdate,enddate,status,setPname,setDiscription,setStartDate,setEndDate,setStatus }) {
    const [pnames,setPnames]=useState('')
    const [discriptions,setDiscriptions]=useState('')
    const [startdates,setStartDates]=useState('')
    const [enddates, setEndDates] = useState();
    const [statuses, setStatuses] = useState();
    
    const [data, setData] = useState('');
    const onSubmitHandler = () => {
        const formData = new FormData();
        formData.append("pname", pnames);
        formData.append("discription", discriptions);
        formData.append("startdate", startdates);
        formData.append("enddate", enddates);
        formData.append("status", statuses);
        axios
            .post(`${process.env.REACT_APP_ERP_URL}/api/project`, formData, {
                method: "POST",
                headers: { "Content-Type": "multipart/form-data" },
            })
            .then((res) => console.log("Successfuly Sent!"))
            .catch((err) => {
                console.log(err);
            });
    };
  
    const onUpdateProjects = (e) => {
        e.preventDefault();
        axios
            .put(`${process.env.REACT_APP_ERP_URL}/api/project/${id}?pname=${pname}&discription=${discription}&startdate=${startdate}&enddate=${enddate}&status=${status}`, {

                headers: { 'Content-Type': 'multipart/form-data' },

            })
            .then((res) => {
                console.log('Successfuly Sent!')
                window.location.reload(false)
            })
            .catch((err) => {

                console.log(err);
            });

    }
   
    if (!id) {
        return (
            <div class="login-box">

                <span className="close" onClick={closeHandler}>
                    &times;
                </span>
                <h1>Assgin Team To Project</h1>
                <hr></hr>
                <form>
                    <div className="user-box">
                        <input type="text" name="pname"
                            value={pnames} onChange={(e) => {
                                setPnames(e.target.value)
                            }}
                            required />
                        <label>Project name</label>
                    </div>
                    <div className="user-box">
                        <input type="text" name="discription"
                            value={discriptions} onChange={(e) => {
                                setDiscriptions(e.target.value)
                            }}
                            required />
                        <label>Description</label>
                    </div>
                    <div className="user-box">
                        <input type="text" name="startdate" placeholder="YYYY-MM-DD"
                            value={startdates} onChange={(e) => {
                                setStartDates(e.target.value)
                            }}
                            required />
                        <label>Start Date</label>
                    </div>
                    <div className="user-box">
                        <input type="text" name="enddate"
                            placeholder="YYYY-MM-DD"
                            value={enddates} onChange={(e) => {
                                setEndDates(e.target.value)
                            }}
                            required />
                        <label>End Date</label>
                    </div>
                    <div className="test">
                        <div>
                <input
                  type="radio"
                  id="active"
                  name="fav_language"
                  
                  value="in process" onChange={(e) => {
                    setStatuses(e.target.value);
                  }}
                />
                <label htmlFor="active">In Process</label>
                <br></br>
                </div>
                <div>
                <input
                  type="radio"
                  id="active"
                  name="fav_language"
                  value="done"
                  
                  onChange={(e) => {
                    setStatuses(e.target.value);
                  }}
                />
                <label htmlFor="not active">Done</label>
                <br></br>
                </div>
              </div>
                    <div>
                        <button className="button-6" role="button" onClick={onSubmitHandler}>ADD</button>

                    </div>

                </form>
            </div>
        )
    } else if (id) {
        return(
        <div class="login-box">
            <span className="close" onClick={closeHandler}>
                &times;
            </span>
            <h1>Assgin Team To Project</h1>
            <hr></hr>
            <form>
                <div className="user-box">
                    <input type="text" name=""
                        value={pname} onChange={(e) => {
                            setPname(e.target.value)
                        }}
                        required />
                    <label>Project name</label>
                </div>
                <div className="user-box">
                    <input type="text" name=""
                        value={discription} onChange={(e) => {
                            setDiscription(e.target.value)
                        }}
                        required />
                    <label>Discription</label>
                </div>
                <div className="user-box">
                    <input type="text" name="" placeholder="YYYY-MM-DD"
                        value={startdate} onChange={(e) => {
                            setStartDate(e.target.value)
                        }}
                        required />
                    <label>Start Date</label>
                </div>
                <div className="user-box">
                    <input type="text" name=""
                        placeholder="YYYY-MM-DD"
                        value={enddate} onChange={(e) => {
                            setEndDate(e.target.value)
                        }}
                        required />
                    <label>End Date</label>
                </div>
                <div className="test">
                <input
                  type="radio"
                  id="active"
                  name="status"
                  checked={status==="in process"}
                  value="in process" onChange={(e) => {
                    setStatus(e.target.value);
                  }}
                />
                <label htmlFor="active">In Process</label>
                <br></br>

                <input
                  type="radio"
                  id="active"
                  name="status"
                  value="done"
                  checked={status==="done"}
                  onChange={(e) => {
                    setStatus(e.target.value);
                  }}
                />
                <label htmlFor="not active">Done</label>
                
              </div>
             
                <div>
                    <button class="button-6" role="button" onClick={onUpdateProjects}>Edit</button>

                </div>

            </form>
        </div>
        )
    }
    
}

export default ProjectAddEdit