import React from 'react'
import axios from "axios"
import {useEffect,useState} from "react"
import Popup from '../Popup/Popup';
import YouCantDelete from '../Team/YouCantDelete';

function AssginTeams({ closeHandler,id,pname }) {
const [teamsandprojects,setTeamsAndProjects]=useState([]);

const [team,setTeam]=useState(1);
const [teamid,setTemId]=useState([]);
const [loading,setLoading]=useState(true)
const [showcant, setShowCant] = useState(false);
const [showdone, setShowDone] = useState(false);
const showPopupCant = () => {
  setShowCant(true);
};
const showPopupDone = () => {
  setShowDone(true);
};
const closeHandlerCant = () => {
  setShowDone(false)
  setShowCant(false);
  document.body.style.close = "auto";
};
    const getteams = async () => {
      
      try {
        const response = await axios.get(`${process.env.REACT_APP_ERP_URL}/api/team`);
        const { data: team } = response;
        setTeam(team)
        setTemId(team[0].id)
        setLoading(false)
        
      } catch (error) {
        console.log(error.message);
      }
      
    };
      useEffect(() => {
        getteams()
      }, [])
     
      const onSubmitHandler = (e) => {
        const formData = new FormData();
        formData.append("team_id", teamid)
        formData.append("project_id", id)
        axios
          .post(`${process.env.REACT_APP_ERP_URL}/api/assginprojects`,formData,{
            headers: { "Content-Type": "multipart/form-data" },
          })
          .then((res) => {
            console.log("SuccesssetSysidfuly Sent!")
            window.location.reload(false)
            
            
          })
          .catch((err) => {
            console.log(err);
          });
      };
     
     
      const checkProjectsAndTeams= async (e)=>{

        let todayDate = new Date().toISOString().slice(0, 10);
    e.preventDefault()
   
   

       await fetch(`${process.env.REACT_APP_ERP_URL}/api/assginprojects?team_id=${teamid}&project_id=${id}`, {
          
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(reponse => {
            if (reponse.ok) {
              return reponse.json();
            } throw reponse;
          }).then(teamsandprojects => {
            setTeamsAndProjects(teamsandprojects)
           
            if(teamsandprojects.status!=""){
                    showPopupCant()
                    
                   }else if(teamsandprojects.get[0].enddate==todayDate||teamsandprojects.get[0].status=="done"){
                     showPopupDone()
                   }else{
                    onSubmitHandler()
                   }
                   
          },[])
      };
  
     if(loading)return "loading.."
    return (
        <div className="login-box">

            <span className="close" onClick={closeHandler}>
                &times;
            </span>
            <h1>Assgin Project To Team</h1>
            <hr></hr>
            <form>
                <div className="user-box">
                    <input type="text" name=""
                           value={pname}
                        required />
                    <label>Project name</label>
                </div>


                <div className="select-projects">

                    <label>Teams</label>

                    <select name="project_id" value={teamid} onChange={(e) => {
                setTemId(e.target.value);
              }} >
                        {
                        team.map((info) =>
                <option key={info.id} value={info.id}>{info.tname}</option>)
              }
                    </select>

                </div>

                <div>
                    <button class="button-6" role="button" onClick={checkProjectsAndTeams}>Assgin</button>

                </div>

            </form>
            <Popup
          show={showcant}
          setShow={setShowCant}
          content={
            <YouCantDelete 
             closeHandler={closeHandlerCant}
             content="This Project Already Assgined To This Team."
             
            />
          }
        />
         <Popup
          show={showdone}
          content={
            <YouCantDelete 
             closeHandler={closeHandlerCant}
             content="This Project Either is unvalid or it is done."
             
            />
          }
        />
        </div>
         
    )
}

export default AssginTeams