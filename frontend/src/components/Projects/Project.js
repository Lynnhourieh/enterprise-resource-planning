
import React from "react";
import FilterAltIcon from '@mui/icons-material/FilterAlt';
import { useState, useEffect } from "react";
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import ProjectAddEdit from "./ProjectAddEdit";
import Popup from "../Popup/Popup";
import AssginTeams from "./AssginTeams";
import { useNavigate } from "react-router-dom";
import ProjectCard from "../ProjectCard/ProjectCard";
import Search from "../Search/Search"
import "./Project.css"
import { red } from "@material-ui/core/colors";
function Project() {
  const [data, setData] = useState([]);
  const navigate = useNavigate();
  const [loading, setloading] = useState(true);
  const [show, setShow] = useState(false);
  const [showedit, setShowEdit] = useState(false);
  const [id,setId]=useState('')
  const [pname,setPname]=useState('')
  const [discription,setDiscription]=useState('')
  const [startdate,setStartDate]=useState('')
  const [enddate, setEndDate] = useState();
  const [status, setStatus] = useState();
  const [showassgin, setShowAssgin] = useState(false);
  const showPopupEdit = (id,pname,discription,startdate,enddate,status) => {
    setShowEdit(true);
    setId(id)
    setPname(pname)
    setDiscription(discription)
    setStartDate(startdate)
    setEndDate(enddate)
    setStatus(status)
  };
  const showAssgin = (id,pname) => {
    setShowAssgin(true);
    setId(id)
    setPname(pname)
  };
  const showPopup = () => {
    setShow(true);
  };
  const closeHandler = () => {
    setShowEdit(false)
    setShow(false);
    setShowAssgin(false)
    document.body.style.close = "auto";
  };
  useEffect(() => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/project`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setloading(false);
        setData(data);
      });
  }, []);
  const onSubmitProject = async (e) => {
    e.preventDefault();
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/project`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };
  useEffect(()=>{
    const token=localStorage.getItem('token');
      if(!token) {
        navigate('/login');
      }
  },[])
  if (loading) return "loading...";
  const onSearch = async (e, search) => {
    e.preventDefault();
    await fetch(
      `${process.env.REACT_APP_ERP_URL}/api/searchproject?pname=${search}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };
  const onSubmitFilterDone = async (e) => {
    e.preventDefault();
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/done`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };
  const onSubmitFilterProcessing = async (e) => {
    e.preventDefault();
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/inprocess`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };
  console.log(data)
  return( 
  <>
 
    <div className="container">  
    <div className="projectheader">
        <b>Projects</b>
    </div>
        <button className="button-68 addproject" role="button" onClick={showPopup} ><GroupAddIcon/></button>
        <form onSubmit={onSubmitFilterDone}><button className="button-notaction addproject" role="button" ><FilterAltIcon sx={{color:'red'}}/>Done</button></form>
        <form onSubmit={onSubmitFilterProcessing}><button className="button-action addproject" role="button"  ><FilterAltIcon sx={{color:'green'}}/>Processing</button></form>
        <form onSubmit={onSubmitProject}>
          <button className="button-68 addproject" role="button">
            All
          </button>
        </form>
        <Search  onSearchHandler={onSearch}/>
      
      
   <div className="two">
   
    { 
           data?.map((datas) => 
            <ProjectCard Key={datas.id} id={datas.id} pname={datas.pname} discription={datas.discription} status={datas.status} startdate={datas.startdate} enddate={datas.enddate}  showPopupEdit={showPopupEdit} showAssgin={showAssgin}/> 
          )
        } </div>
        <Popup
          show={show}
          overlay={true}
          content={
            <ProjectAddEdit closeHandler={closeHandler}
            
            />
          }
        />
        <Popup
      show={showedit}
      setShow={setShow}
      overlay={true}
      content={
        <ProjectAddEdit 
         closeHandler={closeHandler}
         id={id}
         pname={pname}
         discription={discription}
         startdate={startdate}
         enddate={enddate}
         status={status}
         setPname={setPname}
         setDiscription={setDiscription}
         setStartDate={setStartDate}
         setEndDate={setEndDate}
         setStatus={setStatus}
        />
      }
    />
    <Popup
          show={showassgin}
          overlay={true}
          content={
            <AssginTeams closeHandler={closeHandler}
            id={id}
            pname={pname}
           />
          }
        
        />
  </div>

    
  </>);
}

export default Project;
