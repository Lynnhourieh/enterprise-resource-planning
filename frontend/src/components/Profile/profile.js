import React, { useEffect } from 'react'
import "./profile.css";
import { BsFillBasket2Fill, BsFillCalendarPlusFill, BsFillPersonLinesFill } from 'react-icons/bs';
import { MdLocationSearching, MdOutlineEmail, MdPublish } from 'react-icons/md';
import { useState } from "react";
import { useNavigate,useLocation } from "react-router-dom";
import axios from "axios";
import ProfileEdit from './ProfileEdit';
import Popup from "../Popup/Popup";

import Evaluation from "../Evaluation/Evaluation";
function Profile() {
  const navigate = useNavigate();
  const [loading, setloading] = useState(true);

  const location = useLocation();
  const [data, setData] = useState([]);

    
  console.log(location.state.id)


const [showEvaluatoin, setShowEvaluation] = useState(false);
const showEvaluatoinpopup = () => {
  setShowEvaluation(true);
};
  
    const [classs, setClasss] = useState(false);
   
    const [show,setShow]=useState(false)
    const showedit=()=>{
      setShow(true)
    }
      const closeHandler = () => {
    setShow(false);
 
    setShowEvaluation(false);
    document.body.style.close = "auto";
  };
  const GetEmployee = (e) => {   
    if(location.state.teamid){
   fetch(`${process.env.REACT_APP_ERP_URL}/api/profile?employeeid=${location.state.id}&teamid=${location.state.teamid}`, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        } throw reponse;
      })
      .then(data => {
        setData(data)
        setloading(false);
        console.log(data)
        if (data[0].sname === "admin") {
          setClasss(true);
        } else {
          setClasss(false);
        }
      })
    }else{
      fetch(`${process.env.REACT_APP_ERP_URL}/api/profile?employeeid=${location.state.id}`, {
        method: 'GET',
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(reponse => {
          if (reponse.ok) {
            return reponse.json();
          } throw reponse;
        })
        .then(data => {
          setData(data)
          setloading(false);
          console.log(data)
          if (data[0].sname === "admin") {
            setClasss(true);
          } else {
            setClasss(false);
          }
        })
    }
  }

  
  useEffect((e) => {
    GetEmployee();
   
    if(location.state==null){
      navigate('/employees');
    }
    
  }, [classs]);

  
 

  if (loading) return "loading...";
  
  if(show===false){
  // return (
  //   <div className='profile'>
  //     <div className="employeeNamecontainer">
  //       <h1 className="employeeName">Employee Profile</h1>
  //     </div>
  //     <div className="profilecontainer">
  //       <div className="employeeInfo">
  //         <div className="employeetop">
  //           <img src={`${process.env.REACT_APP_ERP_URL}/images/${data[0].image}`} alt="" className="userimg" />
  //           <div className="usershowtoptitle">
  //             <span className="profileshowusername">Name: {data[0].fname}  {data[0].lname}</span>
  //             <span className="profileshowusername">Role: {data[0].sname}</span>

  //           </div>
  //         </div>
  //         <div className='info'>
  //         <div className="employeebuttom">
  //           <div className="span employeeshowetitle">Employee Details</div>
  //           <div className="employeeinfo">
  //             <MdOutlineEmail className='employeedetailesicon' />
  //             <div className="span employeeusername">{data[0].fname} {data[0].mname} {data[0].lname}</div>
  //           </div>
  //           <div className="employeeinfo">
  //             <BsFillPersonLinesFill className='employeedetailesicon' />
  //             <div className="span employeeusername">{data[0].tname}</div>
  //           </div>
  //           {classs ?

  //             <div className="employeeinfo">
  //               <MdOutlineEmail className='employeedetailesicon' />
  //               <div className="span employeeusername">{data[0].username}</div>
  //             </div>
  //             : ""}
  //           {classs ? <div className="employeeinfo">
  //             <BsFillPersonLinesFill className='employeedetailesicon' />
  //             <div className="span employeeusername">{data[0].password}</div>
  //           </div>

  //             : ""}
  //           <div className="employeeinfo">
  //             <MdOutlineEmail className='employeedetailesicon' />
  //             <div className="span employeeusername">{data[0].isActive}</div>
  //           </div>

  //         </div>
  //         <div className="employeebuttom">
  //           <div className="span employeeshowetitle">Contact Details</div>
  //           <div className="employeeinfo">
  //             <MdOutlineEmail className='employeedetailesicon' />
  //             <div className="span employeeusername">{data[0].email}</div>
  //           </div>
  //           <div className="employeeinfo">
  //             <BsFillPersonLinesFill className='employeedetailesicon' />
  //             <div className="span employeeusername">{data[0].phone}</div>
  //           </div>
            
  //               <button className='profileupdatebutton' onClick={()=>showedit()}>Update</button>

  //         </div>
  //       </div>
  //       </div>
  //     </div>
  //   </div>

           
  // )
            
  return(
    <div className='profile'>
      <div className="employeeNamecontainer">
      
        <h1 className="employeeName">Employee Profile</h1>
      </div>
      <div className="employeeUpdate">
        <div className="employeeupdate">
          <div className="employee_pro_header">
            <h2 className="employeeupdatetitle">Profile</h2>
          <button className='profileEvaluationbutton' role="button" onClick={showEvaluatoinpopup}>Evaluation</button> 
          </div>
         
          <form className="userupdateform" encType='multipart/form-data'>
            
            <div className="userupdateleft">
              <div className="userinfoitem">
                <label>Name: {data[0].fname} {data[0].mname} {data[0].lname}</label>
               
           
              </div>
              <div className="userinfoitem">
                <label>Email Address: {data[0].email} </label>
              
              </div>
              <div className="userinfoitem">
                <label>Phone: {data[0].phone}</label>
                
              </div>
              <div className="userinfoitem">
                <label>Role: {data[0].sname}</label>
               
              </div>
              <div className="userinfoitem">
                <label>Team: {data[0].tname}</label>
                
              </div>


            </div>
            <div className="userupdateright">
              {classs ?
                <div className="userinfoitem">
                  <label>Username: {data[0].username} </label>
             
                </div>
                : ""}
              {classs ?
                <div className="userinfoitem">
                  <label>Password: {data[0].password}</label>
                 
                </div>
                : ""}
              <div className="userinfoitem">

                <div className='radio-button'>
                  <div>
                  
                    <label htmlFor="gender">{data[0].gender}</label>

                  </div>
                 
                </div>
              </div>
              <div className="userinfoitem">

                <div className='radio-button'>
                  <div>
                    
                    <label htmlFor="active">{data[0].isActive}</label>

                  </div>
                  
                </div>
              </div>
              <div className="userupdateupload">
                <img alt="image" className="userupdateimg" src={`${process.env.REACT_APP_ERP_URL}/images/${data[0].image}`} />
                <button className='profileupdatebutton' onClick={()=>showedit()}>Update</button>
                
              </div>
             
            </div>
          </form>
        </div>
      </div>
      







       <Popup
                     show={showEvaluatoin}
                     setShow={setShowEvaluation}
                     content={ <Evaluation  
                       
                       
                       id={location.state.id} fname={data[0].fname} lname={data[0].lname} closeHandler={closeHandler}
                       />
                     }
                     />
    </div>
         
  )

  }else if(show===true){ return <ProfileEdit id={location.state.id} />
}
}

export default Profile