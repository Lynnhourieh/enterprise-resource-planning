import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { MdPublish } from 'react-icons/md';
function ProfileEdit({ id }) {
  const [data, setData] = useState('')
  const [image, setImage] = useState([]);
  const [fname, setFname] = useState([]);
  const [mname, setMname] = useState([]);
  const [lname, setLname] = useState([]);
  const [email, setEmail] = useState([]);
  const [phone, setPhone] = useState([]);
  const [sys_id, setSysid] = useState([]);
  const [team_id, setTeamid] = useState([]);
  const [username, setUsername] = useState([]);
  const [password, setPassword] = useState([]);
  const [gender, setGender] = useState([]);
  const [isActive, setisActive] = useState([]);
  const [team, setTeam] = useState('')
  const [role, setRole] = useState('');
  const [classs, setClasss] = useState(false);
  const [loading, setloading] = useState(true);
  const [loadings, setloadings] = useState(true);
  const GetEmployee = () => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/employee/${id}`, {
      method: 'GET',
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(reponse => {
        if (reponse.ok) {
          return reponse.json();
        } throw reponse;
      })
      .then(data => {

        setData(data)
        setFname(data[0].fname);
        setMname(data[0].mname);
        setLname(data[0].lname);
        setEmail(data[0].email);
        setPhone(data[0].phone);
        setSysid(data[0].sys_id);
        setTeamid(data[0].team_id);
        setUsername(data[0].username);
        setPassword(data[0].password);
        setGender(data[0].gender);
        setisActive(data[0].isActive);

        setloading(false);
        
        if (data[0].sname === "admin") {
          setClasss(true);
        } else {
          setClasss(false);
        }
      })

  }
  const onChangeFile = (e) => {
    setImage(e.target.files[0]);

  }
  const getTeams = async () => {
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/team`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((team) => {
        setTeam(team);

      });
  }

  const getRoles = async () => {
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/sysrole`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((role) => {
        setRole(role);
        setloadings(false)
      });
  }
  useEffect(() => {
    getTeams();
    getRoles();
    GetEmployee();
    
  }, [])

  const onSubmitHandler = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("image", image)
    formData.append("fname", fname)
    formData.append("mname", mname)
    formData.append("lname", lname)
    formData.append("email", email)
    formData.append("phone", phone)
    formData.append("sys_id", sys_id)
    formData.append("team_id", team_id)
    formData.append("username", username)
    formData.append("password", password)
    formData.append("gender", gender)
    formData.append("isActive", isActive)
    formData.append("_method", "PUT");
    axios
      .post(`${process.env.REACT_APP_ERP_URL}/api/employee/${id}`, formData)
      .then((res) => {
        console.log("SuccesssetSysidfuly Sent!")

        window.location.reload(false)
      })
      .catch((err) => {
        console.log(err);
      });

  };
  if (loading) return "loaoding..."
  if (loadings) return "loading..."

  return (
    <div>
      <div className="employeeNamecontainer">
        <h1 className="employeeName">Employee Profile</h1>
      </div>
      <div className="employeeUpdate">
   
            
        <div className="employeeupdate">
          <h2 className="employeeupdatetitle">Edit</h2>
          <form className="userupdateform" encType='multipart/form-data'>
            <div className="userupdateleft">
              <div className="userinfoitem">
                <label>First Name:</label>
                <input type="text" name="fname" value={fname} onChange={(e) => { setFname(e.target.value) }} className='userupdateinput' />
              </div>
              <div className="userinfoitem">
                <label>Middle Name:</label>
                <input type="text" name="mname" value={mname} onChange={(e) => { setMname(e.target.value) }} className='userupdateinput' />
              </div>
              <div className="userinfoitem">
                <label>Last Name:</label>
                <input type="text" name="lname" value={lname} onChange={(e) => { setLname(e.target.value) }} className='userupdateinput' />
              </div>
              <div className="userinfoitem">
                <label>Email Address:</label>
                <input type="text" name="email" value={email} onChange={(e) => { setEmail(e.target.value) }} className='userupdateinput' />
              </div>
              <div className="userinfoitem">
                <label>Phone</label>
                <input type="text" name="phone" value={phone} onChange={(e) => { setPhone(e.target.value) }} className='userupdateinput' />
              </div>
              <div className="userinfoitem">
                <label>Role:</label>
                <select type="nyc" name="sys_id" value={sys_id} onChange={(e) => { setSysid(e.target.value) }} className='userupdateinput' >
                  {
                    role.map((info) =>
                      <option key={info.id} value={info.id}>{info.sname}</option>
                    )
                  }
                </select>
              </div>
              <div className="userinfoitem">
                <label>Team:</label>
                <select type="nyc" name="team_id" value={team_id} onChange={(e) => { setTeamid(e.target.value) }} className='userupdateinput' >
                  {team.map((info) =>
                    <option key={info.id} value={info.id}>{info.tname}</option>
                  )
                  }
                </select>
              </div>


            </div>
            <div className="userupdateright">
              {classs ?
                <div className="userinfoitem">
                  <label>Username</label>
                  <input type="text" name="username" value={username} onChange={(e) => { setUsername(e.target.value) }} className='userupdateinput' />
                </div>
                : ""}
              {classs ?
                <div className="userinfoitem">
                  <label>Password:</label>
                  <input type="text" name="password" value={password} onChange={(e) => { setPassword(e.target.value) }} className='userupdateinput' />
                </div>
                : ""}
              <div className="userinfoitem">

                <div className='radio-button'>
                  <div>
                    <input
                      type="radio"
                      id="male"
                      name="male"
                      checked={gender === "male"}
                      value="male" onChange={(e) => {
                        setGender(e.target.value);
                      }}
                    />
                    <label htmlFor="male">Male</label>

                  </div>
                  <div>
                    <input
                      type="radio"
                      id="female"
                      name="femalez"
                      value="female"
                      checked={gender === "female"}
                      onChange={(e) => {
                        setGender(e.target.value);
                      }}
                    />
                    <label htmlFor="female">Female</label>
                  </div>
                </div>
              </div>
              <div className="userinfoitem">

                <div className='radio-button'>
                  <div>
                    <input
                      type="radio"
                      id="active"
                      name="fav_language"
                      checked={isActive === "active"}
                      value="active" onChange={(e) => {
                        setisActive(e.target.value);
                      }}
                    />
                    <label htmlFor="active">Active</label>

                  </div>
                  <div>
                    <input
                      type="radio"
                      id="active"
                      name="fav_language"
                      value="not active"
                      checked={isActive === "not active"}
                      onChange={(e) => {
                        setisActive(e.target.value);
                      }}
                    />
                    <label htmlFor="not active">Not Active</label>
                  </div>
                </div>
              </div>
              <div className="userupdateupload">
                <img alt="image" className="userupdateimg" src={(image != "") ? URL.createObjectURL(image) : `${process.env.REACT_APP_ERP_URL}/images/${data[0].image}`} />
                <label htmlFor="file"><MdPublish className='imgupdatecursor' /></label>
                <input type="file" id="file" name="image" style={{ display: "none" }} onChange={onChangeFile} />
              </div>
              <button className='profileupdatebutton' onClick={onSubmitHandler}>Update</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ProfileEdit