import { Outlet } from "react-router-dom";
import Navbar from "../NavBar/Navbar";
import Sidebar from "../SideBar/sidebar";
import '../../Page.css'
import { useEffect } from "react";
const MainLayout = () => {
   
    return (<>
        <Navbar className="header-dash" />
        <div className="dash-raffle">
            <div className="dash-side">
                <Sidebar  />
            </div>

            <div className='show-profile'>
                <Outlet />
            </div>
        </div>
    </>)
}

export default MainLayout;