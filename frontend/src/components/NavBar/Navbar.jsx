import React from 'react';
import './Navbar.css'
import logo from "./codi-20bootcamp-20logo-removebg-preview.png"
import { BiLogOut } from "react-icons/bi"
import { useNavigate } from "react-router-dom";
export default function Navbar() {
  const navigate = useNavigate();
const onLogout=()=>{
  localStorage.removeItem('token');
  localStorage.removeItem('id');
    navigate('/login');
  
}

  return (
    <div className='navbar'>
        <div className="logo">
            <img src={logo} className='logoimage' alt="logo" />Codi</div>
      
        <div className="empty"></div>
        <div className=" button-68 logout" onClick={onLogout}>
        <BiLogOut />
          </div>

    </div>
  )
}
