import React, { useEffect } from 'react'

import EmployeeRoleMapping from './EmployeeRoleMapping';
import { useLocation,useNavigate } from "react-router-dom";
import {useState}from "react"



function RoleDetails() {
    const [dataemployee,setDataEmployee]=useState([])
   
    const navigate = useNavigate();
    const location = useLocation();
   
  
    const getEmployees=async (e)=>{
        
        await fetch(`${process.env.REACT_APP_ERP_URL}/api/employeesroles?id=${location.state.id}`, {
          
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(reponse => {
            if (reponse.ok) {
              return reponse.json();
            
            } 
          }).then(dataemployee => {
            setDataEmployee(dataemployee)
           console.log(dataemployee)
          },[])
      };
   
      useEffect(()=>{
        if(location.state===null){
        navigate('/roles')
        }
    
      },[])
   
    return (
        <div className='wraping-page'>
            
            <div className='projectheader'>
                <div>
                     {location.state.Rname}
                
                </div>
            </div>
            <div>
             
                <EmployeeRoleMapping id={location.state.id} getEmployees={getEmployees} dataemployee={dataemployee}  />
              
            </div>
            
           
        
        </div>
    )
}

export default RoleDetails