import React from "react";
import CircleIcon from "@mui/icons-material/Circle";
import { useState, useEffect } from "react";
import {useNavigate} from "react-router-dom";
import axios from "axios"
import { BsWindowSidebar } from "react-icons/bs";
function EmployeeRoleCards ({id,fname,image,lname,isActive,startdate,enddate,projectid,pname}){
    const [data, setData] = useState([isActive]);
    const [dataemployee, setDataEmployee] = useState([]);
    const [rname, setRname] = useState([]);
    const [classs, setClasss] = useState(false);
    const navigate=useNavigate()
    
    const Profile=()=>{
        navigate("/profile",{state:{id:id}})
    }
    const projectDetails=()=>{
      navigate("/projectdetails",{state:{id:projectid,pname:pname}})
  }
    // const onSubmitHandler = (e) => {
    //   e.preventDefault()
    //     let todayDate = new Date().toISOString().slice(0, 10);
    //     e.preventDefault()
    //     const formData = new FormData();
    //     formData.append("enddate", todayDate);
    //     formData.append("_method", "PUT");
    //     axios
    //       .post(`${process.env.REACT_APP_ERP_URL}/api/updateemployeerole/${projectid}/${id}`,formData,{
    //         headers: { "Content-Type": "multipart/form-data" },
    //       })
    //       .then((res) => {
    //         console.log("SuccesssetSysidfuly Sent!")
    //         window.location.reload(false)
    //       })
    //       .catch((err) => {
    //         console.log(err);
           
    //       });
    //   };
//     useEffect(()=>{
// checkDate()
//     },[classs])
// const checkDate = () => {
//     if(enddate!=null){
//         setClasss(true)
//     }else{
//         setClasss(false)
//     }
//   };
    return(
        <div class="container">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
          <div class="our-team">
            <div class="picture">
              <img src={`${process.env.REACT_APP_ERP_URL}/images/${image}`} />
            </div>
            <div class="team-content">
              <h3 class="nameemployee" onClick={Profile}>
                {fname} {lname}{" "}
                {data == "active" ? (
                  <CircleIcon sx={{ color: "green" }} />
                ) : (
                  <CircleIcon style={{ color: "red" }} />
                )}
              </h3>
  
            
              <h5 class="titleemployee" onClick={projectDetails}>{pname}</h5>
              <h5 class="titleemployee">{startdate}</h5>
             
            </div>
          </div>
        </div>
        </div>
   )
}
export default EmployeeRoleCards;