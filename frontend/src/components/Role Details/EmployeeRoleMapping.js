import React, { useEffect } from 'react'
import EmployeeRoleCards from './EmployeeRoleCards'
import {useState} from "react"

function EmployeeRoleMapping({getEmployees,dataemployee}) {

 useEffect(()=>{
getEmployees()

 },[])
console.log(dataemployee)
  return (
    <div className="team-cards">

{
       dataemployee?.map((datas,index) => 
       <EmployeeRoleCards key={index} id={datas.employee_id} projectid={datas.project_id} fname={datas.fname} lname={datas.lname} image={datas.image} isActive={datas.isActive} startdate={datas.startdate} enddate={datas.enddate} pname={datas.pname}/> 
     )
       } 
        </div>
  )
}

export default EmployeeRoleMapping