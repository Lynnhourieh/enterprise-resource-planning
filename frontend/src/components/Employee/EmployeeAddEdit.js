import "./employeeAddEdit.css";
import React from "react";
import PersonAddIcon from '@mui/icons-material/PersonAdd';
import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';
import { useState, useEffect } from "react";
import axios from "axios";
function EmployeeAddEdit({ closeHandler }) {
  const [data, setData] = useState([]);
  const [datas, setDatas] = useState([]);
  const [image, setImage] = useState([]);
  const [fname, setFname] = useState([]);
  const [mname, setMname] = useState([]);
  const [lname, setLname] = useState([]);
  const [email, setEmail] = useState([]);
  const [phone, setPhone] = useState([]);
  const [sys_id, setSysid] = useState(1);
  const [team_id, setTeamid] = useState([]);
  const [username, setUsername] = useState([]);
  const [password, setPassword] = useState([]);
  const [gender, setGender] = useState([]);
  const [isActive, setisActive] = useState([]);
  const [classs, setClasss] = useState(false);
  const onChangeFile = e => {

    setImage(e.target.files[0]);

  }
  const onSubmitHandler = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("image", image)
    formData.append("fname", fname)
    formData.append("mname", mname)
    formData.append("lname", lname)
    formData.append("email", email)
    formData.append("phone", phone)
    formData.append("sys_id", sys_id)
    formData.append("team_id", team_id)
    formData.append("username", username)
    formData.append("password", password)
    formData.append("gender", gender)
    formData.append("isActive", isActive)

    axios
      .post(`${process.env.REACT_APP_ERP_URL}/api/employee`, formData, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        console.log("SuccesssetSysidfuly Sent!")
        window.location.reload(false)
      })
      .catch((err) => {
        console.log(err);
      });
  };
  
  const getTeams = () => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/team`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {

        setData(data);
      });
  }

  const getRoles = () => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/sysrole`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((datas) => {

        setDatas(datas);
      });
  }

  useEffect(() => {
    getTeams();
    getRoles();
  }, [classs]);
  // const onClick = (e) => {
  //   if (e === 'admin') {
  //     setClasss(true);
  //   } else {
  //     setClasss(false);
  //   }
  // };
  const onChangeTeam = (e) => {
    setTeamid(e.target.value);
  };
  const onChangeRole = (e) => {
    setSysid(e.target.value);
    if (e.target.value == 2) {
      setClasss(true);
    } else {
      setClasss(false);
    }
  };

  return (
    <div className="Add-Box">
      <span className="close" onClick={closeHandler}>
        &times;
      </span>
      <h1><PersonAddIcon />Add New Employee</h1>
      <hr></hr>
      <form>
        <nav className="all">
          <span className="row1">
            <div className="row">
              <div className="input-Box">
                <label>First Name:</label>
                <br></br>
                <input type="text" name="" value={fname} onChange={(e) => {
                  setFname(e.target.value);
                }} required />
              </div>
              <div className="input-Box middle">
                <label>Middle Name:</label>
                <br></br>
                <input type="text" name="" value={mname} onChange={(e) => {
                  setMname(e.target.value);
                }} required />
              </div>
            </div>
            <div className="input-Box">
              <label>Last Name:</label>
              <br></br>
              <input type="text" name="" required value={lname} onChange={(e) => {
                setLname(e.target.value);
              }} />
            </div>
            <div className="input-Box ">
              <label>Email Adress:</label>
              <br></br>
              <input className="box" type="text" name="" required value={email} onChange={(e) => {
                setEmail(e.target.value);
              }} />
            </div>
            <div className="input-Box">
              <label>Phone:</label>
              <br></br>
              <input className="box" type="text" name="" value={phone} onChange={(e) => {
                setPhone(e.target.value);
              }} required />
            </div>
            <div className="input-Box">

              <br></br>
              <input type="file" name="image" onChange={(
                onChangeFile)} />
            </div>
          </span>
          <span className="row2">

            <label className="role">Role:</label>
            <select name="nyc" className="input-Box box select" value={sys_id} onChange={onChangeRole}>
              {datas.map((info) =>
                <option key={info.id} value={info.id}>{info.sname}</option>)
              }

            </select>
            <label>Team:</label>
            <select name="nyc" className="input-Box box select" value={team_id} onChange={onChangeTeam}>
              {data.map((info) =>
                <option key={info.id} value={info.id}>{info.tname}</option>)
              }
            </select>


            {classs ? <div className="input-Box">
              <label>Username:</label>
              <br></br>
              <input type="text" className="box" name="" value={username} onChange={(e) => {
                setUsername(e.target.value);
              }} />
            </div> : ""}
            {classs ? <div className="input-Box">
              <label>Password:</label>
              <br></br>
              <input type="password" className="box" name="" value={password} onChange={(e) => {
                setPassword(e.target.value);
              }} />
            </div> : ""}


            <div className="input-Box">
              <label>Status:</label>
              <br></br>
              <div className="test">
                <input
                  type="radio"
                  id="male"
                  name="male"
                  value="male" onChange={(e) => {
                    setGender(e.target.value);
                  }}
                />
                <label htmlFor="male">Male</label>
                <br></br>

                <input
                  type="radio"
                  id="female"
                  name="female"
                  value="female"
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                />
                <label htmlFor="female">female</label>
                <br></br>
              </div>
              <div className="test">
                <input
                  type="radio"
                  id="active"
                  name="fav_language"
                  value="active" onChange={(e) => {
                    setisActive(e.target.value);
                  }}
                />
                <label htmlFor="active">Active</label>
                <br></br>

                <input
                  type="radio"
                  id="active"
                  name="fav_language"
                  value="not active"
                  onChange={(e) => {
                    setisActive(e.target.value);
                  }}
                />
                <label htmlFor="not active">Not Active</label>
                <br></br>
              </div>
            </div>
          </span>
        </nav>
        <br></br>
        <button className="button-6 submit" role="button" onClick={onSubmitHandler}> Submit</button>
      </form>
    </div>
  );
}

export default EmployeeAddEdit;
