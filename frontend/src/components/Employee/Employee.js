
import { useState, useEffect } from "react";
import GroupAddIcon from "@mui/icons-material/GroupAdd";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import { useNavigate } from "react-router-dom";
import EmployeeAddEdit from "./EmployeeAddEdit";
import Popup from "../Popup/Popup";
import Pagination from "../Pagination/Paginations";
import EmployeeCard from "../EmployeeCard/EmployeeCard";
import Search from "../Search/Search";
import AllOutIcon from "@mui/icons-material/AllOut";
import "./Employee.css";

function Employee() {
  const [classs, setClasss] = useState(false);
  const [data, setData] = useState([]);
  const [showassgin, setShowAssgin] = useState(false);
  const navigate = useNavigate();
  const [show, setShow] = useState(false);
  const [id, setId] = useState("");
  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [team_id, setTeamid] = useState(1);
  const [team, setTeam] = useState("");
  const [project,setProject]=useState([])
  const [loading, setloading] = useState(true);
  const [currentPage, setCurrentPage] = useState(1);
    const [PerPage, setPerPage] = useState(4);
    const indexOfLast = currentPage * PerPage;
    const indexOfFirst = indexOfLast - PerPage;
  const showPopup = () => {
    setShow(true);
  };
  const closeHandler = () => {
    setShowAssgin(false);
    setShow(false);
    document.body.style.close = "auto";
  };
  const showAssgin = (id, fname, lname) => {
    setShowAssgin(true);
    setId(id);
    setFname(fname);
    setLname(lname);
  };


  useEffect(() => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/employee`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
      fetch(`${process.env.REACT_APP_ERP_URL}/api/project`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      })
        .then((reponse) => {
          if (reponse.ok) {
            return reponse.json();
          }
          throw reponse;
        })
        .then((project) => {
          setProject(project);
        });
  }, []);
  const onSearchHandler = async (e, search) => {
    e.preventDefault();
    await fetch(
      `${process.env.REACT_APP_ERP_URL}/api/search?fname=${search}&lname=${search}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };

  const onSubmitFilterActive = async (e) => {
    e.preventDefault();
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/active`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };
  const onSubmitFindemployee = async (e,id) => {
    e.preventDefault();
    
    await fetch(
      `${process.env.REACT_APP_ERP_URL}/api/findemployee?team_id=${id}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };
  const onSubmitFindProjectEmployee = async (e,id) => {
    e.preventDefault();
    await fetch(
      `${process.env.REACT_APP_ERP_URL}/api/filterproject?project_id=${id}`,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      }
    )
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
        console.log(data)
      });
  };
  
  const onSubmitFilternotActive = async (e) => {
    e.preventDefault();
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/notactive`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
      });
  };
  const onSubmitAllemployee = async (e) => {
    e.preventDefault();
    await fetch(`${process.env.REACT_APP_ERP_URL}/api/employee`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
        
      });
  };
 
  const getTeams = async () => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/team`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setloading(false);
        setTeam(data);
      });
  };
  
  useEffect(() => {
    getTeams();
  }, []);
  
  // const tokenRemove = () => {
  //   localStorage.removeItem('token');
  //   setToken(localStorage.getItem('token'))
  //   console.log(!token);
  //   if(!token) {
  //     navigate('/login');
  //   }
  // }

  // useEffect(() => {
  //   setToken(localStorage.getItem('token'))
  //   setInterval(()=>{
  //     tokenRemove();
  //   }, 43200000)

  // }

  //   , [])
  useEffect(() => {
    const token = localStorage.getItem("token");
    if (!token) {
      navigate("/login");
    }
  }, []);
  const onChangeTeam = (id) => {
    setTeamid(id);
  };
  if (loading) return "loading...";
  return (
    <div className="containeremployee">
      <div></div>
      <div>
        <br></br>

        <div className="Employeeheader">
          <div className="table-header">
            <div >
              <b>Employees</b>
            </div>

            <Search className="search" onSearchHandler={onSearchHandler} />
            <div className="flex-active">
              <form onSubmit={onSubmitFilterActive}>
                <button className="button-action" role="button">
                  <FilterAltIcon sx={{ color: "green" }} />
                  Active
                </button>
              </form>
              <form onSubmit={onSubmitFilternotActive}>
                <button className="button-notaction" role="button">
                  <FilterAltIcon sx={{ color: "red" }} />
                  Not Active
                </button>
              </form>
              <form onSubmit={onSubmitAllemployee}>
                <button className="button-68" role="button">
                  All
                </button>{" "}
              </form>
            </div>
            <div className="flex-search">
              <button className="button-68" role="button" onClick={showPopup}>
                <GroupAddIcon sx={{ color: "black", fontSize: 30  }} />
              </button>

              <select
                name="nyc"
                className="teamlist button-68"
                onClick={(e)=>onSubmitFindProjectEmployee(e,e.target.value)}
              >
                {project.map((info) => (
                  <option key={info.id} value={info.id} >
                    {info.pname}
                  </option>
                ))}

</select>
              <select
                name="nyc"
                className="teamlist button-68"
                value={team_id}
                onChange={(e)=>onChangeTeam(e.target.value)}
                onClick={(e)=>onSubmitFindemployee(e,e.target.value)}
              >
                {team.map((info) => (
                  <option key={info.id} value={info.id} >
                    {info.tname}
                  </option>
                ))}
              </select>
            </div>
          </div>
        </div>

        <div className="one">
          {
          
          data.slice(indexOfFirst, indexOfLast).map((datas) => (
            <EmployeeCard
              key={datas.id}
              id={datas.id}
              fname={datas.fname}
              lname={datas.lname}
              isActive={datas.isActive}
              image={datas.image}
              phone={datas.phone}
              email={datas.email}
              teamid={datas.team_id}
              showAssgin={showAssgin}
            />
          ))}
        </div>

        <Popup
          show={show}
          setShow={setShow}
          content={<EmployeeAddEdit closeHandler={closeHandler} />}
        />
        {/* <Popup
          show={showassgin}
          overlay={true}
          content={
            <AssginEmployeeRoles closeHandler={closeHandler}
            id={id}
            fname={fname}
            lname={lname}
            />
          }
        /> */}
      </div>
      <Pagination
              totalPosts={data.length}
              postsPerPage={PerPage}
              setCurrentPage={setCurrentPage}
              currentPage={currentPage}/>
    </div>
  );
}

export default Employee;
