import React from "react";
import { useState, useEffect } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import "./boxes.css";
import { borderRadius, fontSize } from "@mui/system";
import PersonIcon from "@mui/icons-material/Person";
import GroupsIcon from '@mui/icons-material/Groups';
import CoPresentIcon from '@mui/icons-material/CoPresent';


function Boxes() {
  const [data, setData] = useState([]);
  const [loading, setloading] = useState(true);
  useEffect(() => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/count`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setloading(false);
        setData(data);
       
      });
  }, []);


  if (loading) return "loading...";
  return (

  <> 
    <div className="boxes">

     <div className="box_main">
      <div className="name_total">
        <p className="nameofgroup">
          <b>Total Employee </b>
        </p>
       
        <p className="number">{data.employee}</p>
         
        
       
        </div>
        
      <PersonIcon sx={{ fontSize: "80px" }} />
      </div>

      

     <div className="box_main">
      <div className="name_total">
        <p className="nameofgroup">
          <b>Total Employee </b>
        </p>
       
        <p className="number">{data.team}</p>
         
        
       
        </div>
        
      <GroupsIcon sx={{ fontSize: "80px" }} />
      </div>

      

     <div className="box_main">
      <div className="name_total">
        <p className="nameofgroup">
          <b>Total Employee </b>
        </p>
       
        <p className="number">{data.project}</p>
         
        
       
        </div>
        
      <CoPresentIcon sx={{ fontSize: "80px" }} />
      </div>
      
    
    </div>

  </> 
  
  );
}

export default Boxes;
