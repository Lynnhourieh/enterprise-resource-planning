import React from "react";
import { useState,useEffect } from "react";
import "./ProjectCard.css";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import Popup from "../Popup/Popup"
import axios from "axios"
import YouCantDelete from "../Team/YouCantDelete";
import DeleteConfermation from "../Team/DeleteConfermation"
import { useNavigate } from "react-router-dom"
function ProjectCard({ id,pname, status, startdate, enddate, discription,showPopupEdit,showAssgin }) {
  // const [projectStatus, setProjectStatus] = useState([status]);

  const [showdelete, setShowDelete] = useState(false);
  const [showcant, setShowCant] = useState(false);
  const [data, setData] = useState("");
  const [count,setCount]=useState("")
  
  // const [data1, setData1] = useState([status]);
  const navigate = useNavigate();
  const teamDetails = (id,tname) => {
    navigate('/teamdetails', { state: { id: id,tname:tname } })

  }
  
  const projectDetails = () => {
    navigate('/projectdetails', { state: { id: id,pname: pname } })

  }
  const showPopupDelete = () => {
    setShowDelete(true);
  };
  const showPopupCant = () => {
    setShowCant(true);
  };
  
  const closeHandlerCant = () => {
    setShowCant(false);
    document.body.style.close = "auto";
  };
 
  const closeHandlerDelete = () => {
    setShowDelete(false);
    document.body.style.close = "auto";
  };
  const onSubmitHandler = (e) => {
    fetch(`${process.env.REACT_APP_ERP_URL}/api/getprojects/${id}`, {
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((reponse) => {
        if (reponse.ok) {
          return reponse.json();
        }
        throw reponse;
      })
      .then((data) => {
        setData(data);
        if (data.status!= "") {
          showPopupCant();
        } else {
          return showPopupDelete();
        }
      }, []);
  };

  const onDeleteProject = (e) => {
    e.preventDefault();
    axios
      .delete(`${process.env.REACT_APP_ERP_URL}/api/project/${id}`, {
        headers: { "Content-Type": "multipart/form-data" },
      })
      .then((res) => {
        console.log("Successfuly Sent!");
        window.location.reload(false);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const CountEmployeesandTeams = async () => {

    if (id) {

      try {
        const response = await axios.get(`${process.env.REACT_APP_ERP_URL}/api/countemployeeproject?project_id=${id}`);
        const { data: count } = response;
        setCount(count)
         
      } catch (error) {

        console.log(error.message);
      }
    }
  };

  useEffect(()=>{
CountEmployeesandTeams()
  },[])
  return (
    <div className="container" >
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
        <div class="our-project">
        <div className="progress-container2">
             <EditIcon onClick={() => showPopupEdit(id,pname,discription,startdate,enddate,status)}/>
             <DeleteIcon onClick={onSubmitHandler}/></div>
          
          <div class="team-content" onClick={projectDetails}>
            <h3 class="nameemployee" >
             {pname}
            </h3>

            {status =="done" ?(<h5 style={{color:"red"}}class="titleemployee">{status}</h5>):(<h5 style={{color:"green"}}class="titleemployee">{status}</h5>)}
            <h5 class="titleemployee">{startdate} Till {enddate}</h5>
            <h4>Employees: {count.numberofemployees}</h4>
          {count?.teams?.map((info)=>(
          <button className="button-teams" key={info.id} value={info.id} onClick={(e)=>teamDetails(e.target.value,info.tname)}>{info.tname}</button>
           ) )
}
            
          </div>
          </div>
        </div>
      <Popup
        show={showdelete}
        setShow={setShowDelete}
        content={
          <DeleteConfermation
            closeHandlerDelete={closeHandlerDelete}
            onDeleteTeam={onDeleteProject}
            content="Are You Sure You Want To Delete This Project."
            
          />
        }
      />
    <Popup
      show={showcant}
      setShow={setShowCant}
      overlay={true}
      content={
        <YouCantDelete 
         closeHandler={closeHandlerCant}
         content="This Project Has Teams You Can't Delete It."
        />
      }
    />
    </div>
  );
}
export default ProjectCard;
