import React, { useEffect } from 'react'
import "./teamdetail.css"
import EmployeeMapping from './EmployeeMapping'
import { useLocation,useNavigate } from "react-router-dom";
import {useState}from "react"
import Popup from '../Popup/Popup';
import AssginProjects from './AssginProjects';
import AssginEmployee from './AssginEmployee'
import { listItemSecondaryActionClasses } from '@mui/material';
function TeamDetails() {
    const [dataemployee,setDataEmployee]=useState([])
    const [showassgin, setShowAssgin] = useState(false);
    const [showassginemployee, setShowAssginEmployee] = useState(false);
    const navigate = useNavigate();
    const location = useLocation();
   
    const closeHandler = () => {
     setShowAssginEmployee(false)
      setShowAssgin(false)
      document.body.style.close = "auto";
    };
    const showAssgin = () => {
      setShowAssgin(true);
   
    };
    const showAssginEmployee = () => {
      setShowAssginEmployee(true);
   
    };
    const getEmployees=async (e)=>{
        
        await fetch(`${process.env.REACT_APP_ERP_URL}/api/findemployee?team_id=${location.state.id}`, {
          
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(reponse => {
            if (reponse.ok) {
              return reponse.json();
            
            } 
          }).then(dataemployee => {
            setDataEmployee(dataemployee)
            
          },[])
      };
      const getProjects=async (e)=>{
        
        await fetch(`${process.env.REACT_APP_ERP_URL}/api/getprojectsforteams?team_id=${location.state.id}`, {
          
          headers: {
            "Content-Type": "application/json"
          }
        })
          .then(reponse => {
            if (reponse.ok) {
              return reponse.json();
            
            } 
          }).then(dataemployee => {
            setDataEmployee(dataemployee)
            console.log(dataemployee)
          },[])
      };
      useEffect(()=>{
        if(location.state===null){
        navigate('/teams')
        }
      })
   
    return (
        <div className='wraping-page'>
            <h1 className='projectheader'>
                     {location.state.tname}
                </h1>
            <div className='button-details'>
                
                <div>
                    <button type="submit" className="button-68" onClick={getEmployees}>Employees</button>
                    <button type="submit" className="button-68"onClick={getProjects}>Projects</button>
                </div>
                <button type="submit"  className="button-68"onClick={showAssgin}>Assgin Project</button>
                <button type="submit" className="button-68"onClick={showAssginEmployee}>Assgin Employee</button>
            </div>
            <div>
             
                <EmployeeMapping getEmployees={getEmployees} dataemployee={dataemployee} />
              
            </div>
            <Popup
          show={showassgin}
          overlay={true}
          content={
            <AssginProjects closeHandler={closeHandler}
            id={location.state.id}
            />
          }
        />
           <Popup
          show={showassginemployee}
          overlay={true}
          content={
            <AssginEmployee closeHandler={closeHandler}
            id={location.state.id}
            />
          }
        />
        </div>
    )
}

export default TeamDetails