import React, { useEffect } from 'react'
import EmployeeTeamCards from "./EmployeeTeamCards"
import {useState} from "react"
import ProjectTeamCards from './ProjectTeamCards'
function EmployeeMapping({getEmployees,dataemployee}) {

 useEffect(()=>{
getEmployees()

 },[])

  return (
    <div className="team-cards">

{(
dataemployee?.message)?
       dataemployee?.message?.map((datas,index) => 
       <ProjectTeamCards key={index} id={datas.id} teamid={datas.team_id} pname={datas.pname} /> 
     )
       :
       dataemployee?.map((datas,index) => 
       <EmployeeTeamCards key={index} id={datas.id} fname={datas.fname} lname={datas.lname} image={datas.image} isActive={datas.isActive}/> 
     )
       } 
        </div>
  )
}

export default EmployeeMapping