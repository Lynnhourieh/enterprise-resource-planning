import React from "react";
import "./EmployeeTeamCards.css";
import CircleIcon from '@mui/icons-material/Circle';
import { useState, useEffect } from "react";
import {useNavigate} from "react-router-dom";
function EmployeeTeamCards ({id,fname,image,lname,isActive,phone,email}){
    const [data, setData] = useState([isActive]);
    const navigate=useNavigate()

    const Profile=()=>{
        navigate("/profile",{state:{id:id}})
    }
    return(
        <div class="container">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">
          <div class="our-team">
            <div class="picture">
              <img src={`${process.env.REACT_APP_ERP_URL}/images/${image}`} />
            </div>
            <div class="team-content">
              <h3 class="nameemployee" onClick={Profile}>
                {fname} {lname}{" "}
                {data == "active" ? (
                  <CircleIcon sx={{ color: "green" }} />
                ) : (
                  <CircleIcon style={{ color: "red" }} />
                )}
              </h3>
  
              <h5 class="titleemployee">{phone}</h5>
              <h5 class="titleemployee">{email}</h5>
            </div>
          </div>
        </div>
      </div>
   
        // <div class="image-area" onClick={Profile}>
        //     <div class="img-wrapper">
        //         <img src={`${process.env.REACT_APP_ERP_URL}/Image/${image}`}/>

        //         {data=="active" ?  <h2 style={{backgroundColor:'green'}}>{fname} {lname}
        //         </h2> : <h2 style={{backgroundColor:'red'}}>{fname} {lname}</h2>}
               
             
        //     </div>
        // </div>
   )
}
export default EmployeeTeamCards;