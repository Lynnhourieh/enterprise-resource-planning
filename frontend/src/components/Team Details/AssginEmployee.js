import React from 'react'
import { useState, useEffect } from "react"
import axios from "axios"
function AssginEmployee({ id, closeHandler }) {
    const [team, setTeam] = useState('')
    const [employee,setEmployee]=useState('')
    const [employeeid,setEmployeeId]=useState('')
    const [loading,setloading]=useState(true)
    const getteam = async () => {
        if (id != "") {
            try {
                const response = await axios.get(`${process.env.REACT_APP_ERP_URL}/api/team/${id}`);
                const { data: team } = response;
                setTeam(team)

            } catch (error) {
                console.log(error.message);
            }
        }
    };
    const getunassginedemployees = async () => {
      
        try {
          const response = await axios.get(`${process.env.REACT_APP_ERP_URL}/api/unassginedemployees`);
          const { data: employee } = response;
          setEmployee(employee)
          setEmployeeId(employee[0].id)
       
         
          
        } catch (error) {
          console.log(error.message);
        }
        
      };
      const onSubmitHandler = (e) => {
        e.preventDefault()
        const formData = new FormData();
        formData.append("_method", "PUT");
        axios
          .post(`${process.env.REACT_APP_ERP_URL}/api/updateemployee/${employeeid}?team_id=${id}`,formData,{
            headers: { "Content-Type": "multipart/form-data" },
          })
          .then((res) => {
            console.log("SuccesssetSysidfuly Sent!")
            window.location.reload(false)
            
            
          })
          .catch((err) => {
            console.log(err);
           
          });
      };
    useEffect(() => {
    getunassginedemployees()
        getteam()
    }, [])
// if(loading) return "loading..."
    return (
        <div className="login-box">

            <span className="close" onClick={closeHandler}>
                &times;
            </span>
            <h1>Assgin Team To Project</h1>
            <hr></hr>
            <form>
                <div className="user-box">
                    <input type="text" name=""
                        value={team.tname}
                        required />
                    <label>Team name</label>
                </div>


                <div className="select-projects">

                    <label>Projects</label>

                    <select name="project_id" value={employeeid} onChange={(e) => {
                        setEmployeeId(e.target.value);
                    }} >
                        {
                            employee?.message?.map((info) =>
                                <option key={info.id} value={info.id}>{info.fname} {info.lname}</option>)
                        }
                    </select>

                </div>

                <div>
                    <button class="button-6" role="button" onClick={onSubmitHandler}>Assgin</button>

                </div>
                </form>
        </div>
    )
}

export default AssginEmployee