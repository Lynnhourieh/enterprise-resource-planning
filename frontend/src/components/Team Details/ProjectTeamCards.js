import React from 'react'
import DeleteIcon from '@mui/icons-material/Delete';
import axios from "axios"
import { BsWindowSidebar } from 'react-icons/bs';
import {useNavigate} from "react-router-dom"
function ProjectTeamCards({pname,id,teamid}) {
  const navigate = useNavigate();
  const projectDetails = () => {
    navigate('/projectdetails', { state: { id: id,pname:pname } })

  }

    const onSubmitHandler = (e) => {
        
        axios
          .delete(`${process.env.REACT_APP_ERP_URL}/api/deleteassgin?team_id=${teamid}&project_id=${id}`,{
            headers: { "Content-Type": "multipart/form-data" },
          })
          .then((res) => {
            console.log("SuccesssetSysidfuly Sent!")
            window.location.reload(false)
            
            
          })
          .catch((err) => {
            console.log(err);
           
          },[]);
      };
  return (
    <div class="container">
        <div class="col-12 col-sm-6 col-md-4 col-lg-3">

          <div class="our-team">

            <div class="picture">

              <img class="img-fluid" src="https://picsum.photos/130/130?image=839" />
            </div>
            <div class="team-content" >
              <h3 class="nameemployee" onClick={projectDetails}>{pname}</h3>
            </div>
 <div class="delete-button" >
                  <DeleteIcon sx={{color:'#57bee6'}}onClick={onSubmitHandler}/>
                </div>
          </div>

        </div>


      </div>
  )
}

export default ProjectTeamCards
